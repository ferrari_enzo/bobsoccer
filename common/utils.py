from django.core.cache.utils import make_template_fragment_key
from django.core.cache import cache


def delete_cache(name, *args):
    template = make_template_fragment_key(name, args)
    return cache.delete(template)
