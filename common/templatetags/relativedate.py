# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import template
from django.utils import timezone
from django.utils.translation import ugettext as _


register = template.Library()


@register.filter
def reldate(value):
    days = (timezone.now().date() - value).days
    if days == 0:
        return _('Сегодня')
    if days == 1:
        return _('Вчера')
    return value
