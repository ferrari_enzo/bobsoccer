from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render

from blogs.models import Article
from users.forms import AuthForm
from users.models import AlbumImage


def index(request):
    # TODO: optimize
    news = Article.objects.news().order_by('-when_created')[:8]
    opinions = Article.objects.opinions().order_by('-when_created')[:8]
    discuss = Article.objects.discuss().order_by('-when_created')[:3]
    forecast = Article.objects.forecast().order_by('-when_created') \
                   .select_related('user')[:3]
    return render(request, 'index.html', {'articles': {'news': news,
                                                       'opinions': opinions,
                                                       'discuss': discuss,
                                                       'forecast': forecast},
                                          'auth_form': AuthForm()})


def about(request):
    return render(request, 'about.html')


def contacts(request):
    return render(request, 'contacts.html')


def adv(request):
    return render(request, 'adv.html')


def advert(request):
    return render(request, 'advert.html')


def rules(request):
    return render(request, 'rules.html')


def levels_view(request):
    return render(request, 'levels.html')


def stuff(request):
    return render(request, 'stuff.html')


def photo(request):
    images = AlbumImage.objects.filter(show=True)
    paginator = Paginator(images, per_page=25)

    page = request.GET.get('page')
    try:
        images = paginator.page(page)
    except PageNotAnInteger:
        images = paginator.page(1)
    except EmptyPage:
        images = paginator.page(paginator.num_pages)
    context = {
        'photos': images
    }
    return render(request, 'photo.html', context)


def votes(request):
    return render(request, 'votes.html')