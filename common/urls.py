from django.conf.urls import url
from . import views


urlpatterns = [
    url('^$', views.index, name='index'),
    url(r'^about/$', views.about, name='about'),
    url(r'^about/stuff/$', views.stuff, name='stuff'),
    url(r'^about/rules/$', views.rules, name='rules'),
    url(r'^about/levels/$', views.levels_view, name='levels'),
    url(r'^adv/$', views.adv, name='adv'),
    url(r'^advert/$', views.advert, name='advert'),
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^photo/$', views.photo, name='photo'),
    url(r'^votes/$', views.votes, name='votes'),
]
