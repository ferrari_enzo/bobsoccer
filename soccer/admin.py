from django.contrib import admin

from .models import Team, Person
# Register your models here.


class TeamAdmin(admin.ModelAdmin):
    list_display = (u'id', u'name')
    search_fields = (u'name',)
admin.site.register(Team, TeamAdmin)


class PersonAdmin(admin.ModelAdmin):
    list_display = (u'id', u'name', u'type', u'team')
    search_fields = (u'name', u'team')
admin.site.register(Person, PersonAdmin)
