# -*- coding: utf-8 -*-


from __future__ import unicode_literals

from django.db import models
from django.db.models import Count
from django.utils.translation import ugettext as _


class Team(models.Model):
    name = models.CharField(_('Имя'), max_length=255)
    logo = models.ImageField(_('Логотип'), upload_to='team/logos/', null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Команда')
        verbose_name_plural = _('Команды')


class PersonObjects(models.QuerySet):
    def coaches(self, *args, **kwargs):
        kwargs['type'] = 'coach'
        return self.filter(*args, **kwargs)

    def players(self, *args, **kwargs):
        kwargs['type'] = 'player'
        return self.filter(*args, **kwargs)

    def specs(self, *args, **kwargs):
        kwargs['type'] = 'spec'
        return self.filter(*args, **kwargs)


class Person(models.Model):
    class TYPE:
        COACH = 'coach'
        PLAYER = 'player'
        SPEC = 'spec'
        _CHOICES = ((COACH, 'Тренер'),
                    (PLAYER, 'Игрок'),
                    (SPEC, 'Специалист'))

    name = models.CharField(_('Имя'), max_length=255)
    image = models.ImageField(_('Изображение'), upload_to='team/persons')
    type = models.CharField(
        _('Тип'),
        max_length=10,
        choices=TYPE._CHOICES,
        default=TYPE.PLAYER
    )
    info = models.TextField(_('Информация'))
    team = models.ForeignKey(Team, verbose_name=_('Команда'))

    objects = PersonObjects.as_manager()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('Персона')
        verbose_name_plural = _('Персоны')

    def authors(self):
        return self.articles.values('user__username').annotate(
            c=Count('user__username')).values('c', 'user__username')
