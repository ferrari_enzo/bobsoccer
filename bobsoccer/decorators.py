from django.http import Http404


def ajax_post_only(func):
    def wrap(request, *args, **kwargs):
        if request.method != 'POST' and not request.is_ajax():
            raise Http404()
        return func(request, *args, **kwargs)
    return wrap
