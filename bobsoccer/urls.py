"""bobsoccer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles import views
from django.views.generic import TemplateView

from users.views import ckeditor_image_upload_view, ckeditor_image_browse_view
from common.views import index

urlpatterns = [
    url(r'^users/success_signup/$', TemplateView.as_view(template_name='success_signup.html'), name='success_signup'),
    url(r'^admin/', admin.site.urls),
    url(r'^captcha', include('captcha.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/', include('users.urls', namespace='accounts')),
    url(r'^blogs/', include('blogs.urls', namespace='blogs')),
    url(r'^forum/', include('talks.urls', namespace='forum')),
    url(r'^poll/', include('polls.urls', namespace='polls')),
    url(r'^forecast/', include('forecast.urls', namespace='forecast')),
    url(r'^payment/', include('transaction.urls', namespace='transaction')),
    url(r'^shop/', include('shop.urls', namespace='shop')),
    url(r'^ckeditor/upload/$', ckeditor_image_upload_view, name='ckeditor_upload'),
    url(r'^ckeditor/browse/$', ckeditor_image_browse_view, name='ckeditor_browse'),
    url(r'^', include('common.urls', namespace='common'))
]

if settings.DEBUG:
    urlpatterns += [
            url(r'^static/(?P<path>.*)$', views.serve),
        ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
