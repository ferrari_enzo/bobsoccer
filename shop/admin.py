from django.contrib import admin

from . import models


@admin.register(models.Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'image_tag', 'pk')
    fields = ('name', 'description', 'image', 'image_tag',
              'price', 'level', 'expired', 'action', 'admin_only')

    readonly_fields = ('image_tag', )
