# coding=utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _


class ItemQueryset(models.QuerySet):
    def dicts(self, *args, **kwargs):
        query = self.filter(*args, **kwargs)
        return [i.as_dict() for i in query]


class Item(models.Model):
    actions = (
        ('rebuke', u'Замечание'),
        ('warning', u'Предупреждение'),
        ('ban', u'Бан')
    )

    levels = (
        (0, u'Нет'),
        (1, u'Запасной игрок'),
        (2, u'Основной игрок'),
        (3, u'Капитан'),
        (4, u'Судья'),
        (5, u'Корреспондент'),
        (6, u'Кандидат в эксперты серебро'),
        (7, u'Кандидат в эксперты золото'),
        (50, u'Эксперт'),
        (101, u'Дисквалифицированный')
    )

    name = models.CharField(max_length=255, verbose_name=_('Название'))
    description = models.TextField(verbose_name=_('Описание'),
                                   null=True, blank=True)
    image = models.ImageField(upload_to='items/',
                              verbose_name=_('Картинка'))
    price = models.PositiveIntegerField(verbose_name=_('Стоимость'), null=True,
                                        blank=True)
    level = models.PositiveSmallIntegerField(choices=levels,
                                             verbose_name=_('Минимальный уровень'))
    expired = models.PositiveSmallIntegerField(verbose_name=_('Время действия'),
                                               null=True, blank=True)
    action = models.CharField(max_length=11, choices=actions,
                              null=True, blank=True,
                              verbose_name=_('Действие предмета'))
    points = models.IntegerField(null=True, blank=True)
    admin_only = models.BooleanField(default=False, verbose_name=_('Только для администраторов?'))

    objects = ItemQueryset.as_manager()

    def __unicode__(self):
        return self.name

    def image_tag(self):
        return '<img src="%s" style="width: 150px; height: 150px;>' \
               % self.image.url

    image_tag.short_description = 'Текущая картинка'
    image_tag.allow_tags = True

    def as_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'image': self.image.url
        }

    class Meta:
        verbose_name_plural = u'Предметы'
        verbose_name = u'Предмет'


class CartItem(models.Model):
    item = models.ForeignKey(Item, related_name='carts')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='carts')
    quantity = models.PositiveIntegerField(default=1)
