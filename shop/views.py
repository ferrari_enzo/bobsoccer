# coding=utf-8
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render, redirect

from shop.models import Item, CartItem


def shop_view(request):
    context = {
        'items': Item.objects.filter(admin_only=False)
    }
    return render(request, 'shop.html', context)


def cart_view(request):
    cart_sum = request.user.cart_sum()
    if request.method == 'POST':
        if cart_sum <= request.user.balance:
            request.user.buy_items()
            messages.add_message(request, messages.SUCCESS, 'Спасибо за покупку')
            return redirect('accounts:user_items')
        else:
            messages.add_message(request, messages.WARNING, 'Не достаточно денег на балансе.')
    if request.user.balance < cart_sum:
        balance_add = cart_sum - request.user.balance
    else:
        balance_add = None
    return render(request, 'cart.html', {'balance_add': balance_add})


@login_required
def ajax_cart_add(request):
    if request.method != 'POST' or not request.is_ajax():
        raise Http404()

    try:
        item = Item.objects.get(pk=request.POST.get('item'))
    except Item.DoesNotExist:
        return HttpResponse('ItemDoesNotExist')

    cart_item, created = CartItem.objects.get_or_create(item=item, user=request.user)
    if not created:
        cart_item.quantity += 1
        cart_item.save()

    return HttpResponse(request.user.cart_sum())


@login_required
def ajax_cart_remove(request):
    if request.method != 'POST' or not request.is_ajax():
        raise Http404()

    try:
        CartItem.objects.get(pk=request.POST.get('item')).delete()
        return HttpResponse(request.user.cart_sum())
    except CartItem.DoesNotExist:
        return HttpResponse('ItemDoesNotExist')
