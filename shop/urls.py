from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.shop_view, name='base'),
    url(r'^cart/$', views.cart_view, name='cart'),
    url(r'^cart/add/$', views.ajax_cart_add, name='cart_add'),
    url(r'^cart/remove$', views.ajax_cart_remove, name='cart_remove'),
]