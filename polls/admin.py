from django.contrib import admin

from .models import Poll, PollVariant, PollVote

# Register your models here.


class PollVariantInlineAdmin(admin.TabularInline):
    model = PollVariant
    max_num = 10


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_multivariant', 'active', 'close')
    list_filter = ('is_multivariant', 'active', 'close')
    list_editable = ('active', 'close')
    inlines = (
        PollVariantInlineAdmin,
    )


@admin.register(PollVote)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('user', 'when_vote')