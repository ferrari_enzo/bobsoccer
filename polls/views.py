import json

from django.http.response import Http404, HttpResponse, JsonResponse
from django.template.loader import render_to_string

from polls.models import PollVote, Poll, PollVariant


def poll_vote_view(request):
    if not (request.method == 'POST' and request.is_ajax()):
        raise Http404()

    if request.user.is_anonymous:
        return HttpResponse('error')

    data = request.POST.copy()

    try:
        poll = Poll.objects.get(pk=request.POST.get('poll'))
        data.pop('poll')

    except (Poll.DoesNotExist, KeyError):
        return HttpResponse('error')

    vote, created = PollVote.objects.get_or_create(user=request.user, poll=poll)

    votes = []
    for key, value in data.items():
        if 'option' in key:
            votes.append(value)

    variants = PollVariant.objects.filter(poll=poll, id__in=votes).distinct()

    vote.variant.clear()
    vote.variant.add(*variants)
    vote.save()

    return JsonResponse(json.dumps({
        'count': poll.votes_total(),
        'html': render_to_string('poll_vote_ajax.html', {'poll': poll})
    }), safe=False)

