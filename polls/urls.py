from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^vote/$', views.poll_vote_view, name='poll_vote')
]
