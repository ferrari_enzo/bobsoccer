# coding=utf-8
from django import forms

from .models import PollVariant


class PollForm(forms.Form):
    title = forms.CharField(max_length=255, label=u'Заголовок')
    is_multiple = forms.ChoiceField(
        choices=((False, u'Разрешен один вариант ответа'), (True, u'Разрешено несколько вариантов')),
        widget=forms.RadioSelect,
    )


class PollVariantForm(forms.ModelForm):
    class Meta:
        model = PollVariant
        fields = ('text',)


poll_formset = forms.formset_factory(PollVariantForm,
                                     can_delete=True, extra=1,
                                     max_num=10)
