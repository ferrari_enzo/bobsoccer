# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.db.models import Count, Avg

from blogs.models import Article
from users.models import User

from django.utils.translation import ugettext as _


# Create your models here.


class Poll(models.Model):
    CHOICES_COUNT = (
        (False, u'Один вариант'),
        (True, u'Много вариантов')
    )

    article = models.OneToOneField(Article, related_name='poll', null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    title = models.TextField(_('Название опроса'))
    is_multivariant = models.BooleanField(verbose_name='Количество вариантов', choices=CHOICES_COUNT, default=False)
    active = models.BooleanField(default=True)
    close = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Опрос')
        verbose_name_plural = _('Опросы')

    def __unicode__(self):
        return self.title

    def result(self):
        variants = PollVariant.objects.filter(votes__in=self.votes.all())
        return variants.values('id', 'text').annotate(count=Count('id'))

    def votes_total(self):
        return PollVariant.objects.filter(votes__in=self.votes.all()).count()


class PollVariant(models.Model):
    poll = models.ForeignKey(Poll, verbose_name=_('Опрос'), related_name='options')
    text = models.TextField(max_length=100, verbose_name=_('Текст варианта'))

    class Meta:
        verbose_name = _('Вариант опроса')
        verbose_name_plural = _('Варианты опроса')

    def __unicode__(self):
        return '%s: %s' % (self.poll.title, self.text)


class PollVote(models.Model):
    variant = models.ManyToManyField(PollVariant, verbose_name=_('Вариант'), related_name='votes')
    poll = models.ForeignKey(Poll, verbose_name=_('Опрос'), related_name='votes')
    when_vote = models.DateTimeField(verbose_name=_('Дата выбора'), auto_now_add=True)
    user = models.ForeignKey(User, verbose_name=_('Пользователь'))

    class Meta:
        verbose_name = 'Ответ на опрос'
        verbose_name_plural = 'Ответы на опросы'

    def __unicode__(self):
        if self.user:
            return '%s: %s' % (self.user.username, self.poll.title)

        else:
            return 'AnonymousUser: %s' % self.variant.variant_text


PollVoteThrough = PollVote.variant.through
