# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys

import urllib
import urllib2

import re

from django.conf import settings


path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bobsoccer.settings")
sys.path.append(path)

# This is so my local_settings.py gets loaded.
os.chdir(path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

import MySQLdb

from django.contrib.auth.hashers import make_password
from django.db.utils import IntegrityError
from users.models import User, Avatar, Country, City, UserLocation, UserLevel, \
    UserFriendsThrough, FriendRequest, UserIgnoreThrough, UserItem, UserGift
from polls.models import Poll, PollVariant, PollVote, PollVoteThrough
from blogs.models import Article, Tag, ArticleThrough
from soccer.models import Team, Person
from shop.models import CartItem
from django.utils import timezone
from talks.models import Comment, Topic


class Export(object):
    methods = [
        'countries',
        'cities',
        'teams',
        'avatars',
        'users',
        'user_location',
        'user_level',
        'friends',
        'friend_request',
        'user_ignore',
        'persons',
        'articles',
        'tags',
        'blog_polls',
        'poll_variants',
        'poll_answers',
        'user_stuff',
        'stocks'
    ]

    items = {
        'WhistleGold': 35,
        'Ball': 4,
        'YellowCardGold': 34,
        'Tickets': 5,
        'Boots': 11,
        'RedCardGold': 36,
        'YellowCard': 2,
        'Trol': 13,
        'Bayan': 9,
        'Pennant': 8,
        'Whistle': 1,
        'Offtop': 10,
        'Cup': 6,
        'Beer': 7,
        'RedCard': 3,
        'Helicopter': 23,
        'Bunch': 15,
        'Medal': 37,
        'BayanGold': 38,
        'Magistr': 39,
        'Stop': 40,
        'Santagirl': 21,
        'OfftopGold': 41,
        'PennantGold': 42,
        'TrolGold': 43,
        'Car': 14,
        'Shirt': 12,
        'Xtree': 16,
        'Champagne': 24,
        'Oscar': 44,
        'Gladiator': 32,
        'Horseshoe': 25,
        'PennantCSKA': 27,
        'Snowmen': 22,
        'Santa': 20,
        'XballRW': 17,
        'PennantLoko': 29,
        'XballRB': 18,
        'Lion': 30,
        'XballBB': 19,
        'Cap': 45,
        'PennantSpartak': 28,
        'Feather': 46,
        'SMedal': 47,
        'Glasses': 33,
        'PennantZenit': 26,
        'RewardGold': 48,
        'RewardSilver': 49,
        'RewardBronze': 50,
        'Horse': 31,
    }

    def __init__(self):
        database = settings.DB_DUMP

        db = MySQLdb.connect(**database)

        self.cursor = db.cursor(MySQLdb.cursors.DictCursor)

    # decrypting password from md5 and
    # if it is not None make django password
    @staticmethod
    def _md5_decrytion_and_password_create(password):
        website = 'http://md5decryption.com/'
        weburl = urllib.urlencode({'hash': password, 'submit': 'Decrypt+It!'})
        req = urllib2.Request(website)
        try:
            fd = urllib2.urlopen(req, weburl)
            data = fd.read()
            match = re.search(r'(Decrypted Text: </b>)(.+[^>])(</font><br/><center>)', data)
            if match:
                return match.group(2)
            else:
                return None
        except urllib2.URLError:
            return None

    def countries(self):
        objs = []
        self.cursor.execute('SELECT Code, Value FROM ud_Countries')

        for i in self.cursor.fetchall():
            pk = i['Code']
            name = i['Value'].replace('\n', '')
            objs.append(Country(pk=pk, name=name))

        return len(Country.objects.bulk_create(objs))

    def cities(self):
        objs = []
        self.cursor.execute('SELECT Code, Value FROM ud_Cities')

        for i in self.cursor.fetchall():
            pk = i['Code']
            name = i['Value'].replace('\n', '')
            objs.append(City(pk=pk, name=name))

        return len(City.objects.bulk_create(objs))

    def teams(self):
        objs = []
        self.cursor.execute('SELECT Code, Value FROM ud_Teams')

        for i in self.cursor.fetchall():
            pk = i['Code']
            name = i['Value'].replace('\n', '')
            logo = 'team/logos/%s.png' % pk
            objs.append(Team(pk=pk, name=name, logo=logo))

        return len(Team.objects.bulk_create(objs))

    def avatars(self):
        objs = []
        for i in range(1, 32):
            objs.append(Avatar(pk=i, image='avatars/%s.gif' % i))

        return len(Avatar.objects.bulk_create(objs))

    def users(self):
        self.cursor.execute('SELECT us_id, login, name, mail,'
                            ' regtime, mode, sex, photo, passwd'
                            ' FROM Users')

        users = []
        created = 0
        fetch = self.cursor.fetchall()
        l = len(fetch) - 1
        for index, i in enumerate(fetch):
            if index % 100 == 0 and index > 0 or index == l:
                a = User.objects.bulk_create(users)
                created += len(a)
                users = []

            password = self._md5_decrytion_and_password_create(i['passwd'])
            pk = i['us_id']
            photo = i['photo']
            if photo:
                photo = photo.replace('/data/', '')
            avatar = i['mode']
            if not avatar:
                avatar = None
            gender = i['sex'] if i['sex'] else 0
            kwargs = {
                'id': pk,
                'username': i['login'],
                'first_name': i['name'],
                'email': i['mail'],
                'date_joined': i['regtime'],
                'avatar_id': avatar,
                'gender': gender,
                'photo': photo,
                'password': make_password(password)
            }

            self.cursor.execute('SELECT skype, bth_day, bth_month, bth_year,'
                                'country, city, team, descr from UserData '
                                'WHERE us_id = %s' % pk)

            data = self.cursor.fetchall()

            if len(data) == 1:
                data = data[0]

                city = data['city']
                if city == 0:
                    city = None

                country = data['country']
                if country == 0:
                    country = None
                team = data['team']

                if team == 0:
                    team = None

                kwargs['skype'] = data['skype']
                kwargs['country_id'] = country
                kwargs['city_id'] = city
                kwargs['team_id'] = team
                kwargs['descr'] = data['descr']
                try:
                    year = data['bth_year']
                    if not year:
                        year = 2000

                    month = data['bth_month']
                    if not month:
                        month = 1

                    day = data['bth_day']
                    if not day:
                        day = 1

                    kwargs['birthdate'] = timezone.datetime(year, month, day)
                except ValueError:
                    pass

            self.cursor.execute('SELECT Balance FROM UserBalance WHERE us_id = %s' % pk)
            balance = self.cursor.fetchall()
            if len(balance) == 1:
                kwargs['balance'] = balance[0]['Balance']
            users.append(User(**kwargs))

        return created

    def user_location(self):
        self.cursor.execute('SELECT MAX(Loc_Time), us_id, Loc_Code, Loc_Data, '
                            'Loc_X, Loc_Y, Loc_Zoom '
                            'FROM UserLocation GROUP BY us_id')
        objs = []
        for i in self.cursor.fetchall():
            kwargs = {
                'pk': i['Loc_Code'],
                'user_id': i['us_id'],
                'title': i['Loc_Data'],
                'lat': i['Loc_X'],
                'lng': i['Loc_Y'],
                'zoom': i['Loc_Zoom']
            }
            objs.append(UserLocation(**kwargs))

        return len(UserLocation.objects.bulk_create(objs))

    def user_level(self):
        self.cursor.execute('SELECT us_id, Level, Prev_Points, Lev_Points '
                            'FROM UserLevel')
        objs = []
        for i in self.cursor.fetchall():
            kwargs = {
                'user_id': i['us_id'],
                'level': i['Level'],
                'prev_points': i['Prev_Points'],
                'points': i['Lev_Points']
            }
            objs.append(UserLevel(**kwargs))
        return len(UserLevel.objects.bulk_create(objs))

    def friends(self):
        self.cursor.execute('SELECT us_id, us_friend FROM UserFriends')
        objs = []
        for i in self.cursor.fetchall():
            kwargs = {
                'from_user_id': i['us_id'],
                'to_user_id': i['us_friend']
            }
            objs.append(UserFriendsThrough(**kwargs))
        return len(UserFriendsThrough.objects.bulk_create(objs))

    def friend_request(self):
        self.cursor.execute('SELECT FrR_Code, us_id, us_friend, FrR_Time '
                            'FROM UserFriendsRequest')
        objs = []
        for i in self.cursor.fetchall():
            time = timezone.datetime.fromtimestamp(i['FrR_Time'])
            kwargs = {
                'pk': i['FrR_Code'],
                'user_from_id': i['us_id'],
                'user_to_id': i['us_friend'],
                'time': time
            }
            objs.append(FriendRequest(**kwargs))
        return len(FriendRequest.objects.bulk_create(objs))

    def user_ignore(self):
        self.cursor.execute('SELECT us_id, us_ignor FROM UserIgnor')
        objs = []

        for i in self.cursor.fetchall():
            kwargs = {
                'from_user_id': i['us_id'],
                'to_user_id': i['us_ignor']
            }
            objs.append(UserIgnoreThrough(**kwargs))

        return len(UserIgnoreThrough.objects.bulk_create(objs))

    def persons(self):
        types = {
            1: 'coach',
            2: 'player',
            3: 'spec'
        }
        self.cursor.execute('SELECT Per_Code, Per_Name, Per_Image, Per_Type,'
                            ' Per_Info, Per_Team FROM Persons')
        objs = []
        for i in self.cursor.fetchall():
            kwargs = {
                'pk': i['Per_Code'],
                'name': i['Per_Name'],
                'image': i['Per_Image'].replace('/data/person/', 'team/persons/'),
                'type': types[i['Per_Type']],
                'info': i['Per_Info'],
                'team_id': i['Per_Team']
            }
            objs.append(Person(**kwargs))
        return len(Person.objects.bulk_create(objs))

    def articles(self):
        category = {
            1: 'news',
            2: 'interview',
            4: 'note',
            7: 'discuss',
            8: 'announce',
            9: 'forecast',
            10: 'video',
            11: 'initiative',
            12: 'junior',
            13: 'edition',
            14: 'spb',
        }
        objs = []
        items = []
        exists = ','.join([str(i) for i in Article.objects.values_list('pk', flat=True)])
        for year in range(2010, 2016):
            self.cursor.execute(
                'SELECT Blo_Code, Blo_Header, Blo_Anounce, Blo_Text, '
                'Blo_Source, Blo_Time, us_id, Blo_Type, Blo_Public, '
                'Cnt from blogItems_%s where Blo_Code not in (%s)' % (year, exists))
            items.extend(self.cursor.fetchall())
        self.cursor.execute(
            'SELECT Blo_Code, Blo_Header, Blo_Anounce, Blo_Text, '
            'Blo_Source, Blo_Time, us_id, Blo_Type, Blo_Public, '
            'Cnt, Ticker FROM blogItems_2016 where Blo_Code not in (%s)' % exists)
        items.extend(self.cursor.fetchall())
        self.cursor.execute(
            'SELECT Blo_Code, Blo_Header, Blo_Anounce, Blo_Text, '
            'Blo_Source, Blo_Time, us_id, Blo_Type, Blo_Public, '
            'Cnt, Ticker FROM blogItems where Blo_Code not in (%s)' % exists)

        items.extend(self.cursor.fetchall())
        created = 0
        l = len(items) - 1
        for index, i in enumerate(items):
            if index % 1000 == 0 and index > 0 or index == l:
                a = Article.objects.bulk_create(objs)
                created += len(a)
                objs = []
                print(created)

            cat = i['Blo_Type']
            if cat == 0:
                cat = 1

            kwargs = {
                'pk': i['Blo_Code'],
                'title': i['Blo_Header'],
                'announce': i['Blo_Anounce'],
                'text': i['Blo_Text'],
                'source': i['Blo_Source'],
                'when_created': timezone.datetime.fromtimestamp(i['Blo_Time']),
                'user_id': i['us_id'],
                'category': category[cat],
                'not_publish': not i['Blo_Public'],
                'total_views': i['Cnt'],
                'ticker': bool(i.get('Ticker', False))
            }

            self.cursor.execute('SELECT * FROM blogIndex_Audio '
                                'WHERE Blo_Code = %s' % i['Blo_Code'])
            if self.cursor.fetchall():
                kwargs['has_audio'] = True
            self.cursor.execute('SELECT * FROM blogIndex_Video '
                                'WHERE Blo_Code = %s' % i['Blo_Code'])
            if self.cursor.fetchall():
                kwargs['has_video'] = True

            self.cursor.execute('SELECT Per_Code FROM blogIndex_Person '
                                'WHERE Blo_Code = %s' % i['Blo_Code'])
            person = self.cursor.fetchall()
            if person:
                kwargs['person_id'] = person[0]['Per_Code']
            objs.append(Article(**kwargs))
        return created

    def tags(self):
        objs = []
        self.cursor.execute('SELECT Tag_Code, Tag_Name FROM Tags')
        for i in self.cursor.fetchall():
            kwargs = {
                'id': i['Tag_Code'],
                'name': i['Tag_Name']
            }
            objs.append(Tag(**kwargs))
        return len(Tag.objects.bulk_create(objs))

    def tags_index(self):
        objs = []
        self.cursor.execute("SELECT Tag_Code, Item FROM TagsIndex")
        result = 0
        items = self.cursor.fetchall()
        l = len(items) -1
        print(items)
        for index, i in enumerate(items):
            if index % 10 == 0 and index > 0 or index == l:
                a = ArticleThrough.objects.bulk_create(objs)
                objs = []
                result += len(a)
                print(result)
            kwargs = {
                'article_id': i['Item'].split('.')[1],
                'tag_id': i['Tag_Code']
            }
            objs.append(ArticleThrough(**kwargs))

        return result

    def blog_polls(self):
        self.cursor.execute(
            'SELECT Pol_Code, Blo_Code, us_id,'
            ' Pol_Name, Pol_Type, Pol_Time, Pol_Active FROM blogPolls')
        objs = []
        for i in self.cursor.fetchall():
            if i['Pol_Type'] == 2:
                multiple = True
            else:
                multiple = False

            kwargs = {
                'pk': i['Pol_Code'],
                'article_id': i['Blo_Code'],
                'title': i['Pol_Name'],
                'is_multivariant': multiple,
                'date': i['Pol_Time']
            }
            objs.append(Poll(**kwargs))

        self.cursor.execute('SELECT Lst_Code, Lst_Desc, '
                            'Lst_Close, Lst_Active FROM voteList')
        for i in self.cursor.fetchall():
            kwargs = {
                'pk': i['Lst_Code'],
                'title': i['Lst_Desc'],
                'close': i['Lst_Close'],
                'active': i['Lst_Active']
            }
            objs.append(Poll(**kwargs))
        a = Poll.objects.bulk_create(objs)
        return len(a)

    def poll_variants(self):
        objs = []
        self.cursor.execute('SELECT PV_Code, Pol_Code, PV_Value '
                            'FROM blogPollVariants')
        for i in self.cursor.fetchall():
            kwargs = {
                'pk': i['PV_Code'],
                'poll_id': i['Pol_Code'],
                'text': i['PV_Value']
            }
            objs.append(PollVariant(**kwargs))
        return len(PollVariant.objects.bulk_create(objs))

    def poll_answers(self):
        objs = []
        self.cursor.execute('SELECT PA_Code, Pol_Code, PV_Code, us_id'
                            ' FROM blogPollAnswers')
        answers = self.cursor.fetchall()
        objs_through = []
        for i in answers:
            kwargs = {
                'pk': i['PA_Code'],
                'poll_id': i['Pol_Code'],
                'user_id': i['us_id']
            }
            kwargs_through = {
                'pollvote_id': i['PA_Code'],
                'pollvariant_id': i['PV_Code']
            }
            objs.append(PollVote(**kwargs))
            objs_through.append(PollVoteThrough(**kwargs_through))

        votes = PollVote.objects.bulk_create(objs)
        through = PollVoteThrough.objects.bulk_create(objs_through)
        return len(votes) + len(through)

    def stocks(self):

        self.cursor.execute('select St_Code, Count(*) as quantity, '
                            'us_id, St_Class '
                            'from UserStocks '
                            'group by us_id, St_Class;')
        objs = []
        for i in self.cursor.fetchall():
            kwargs = {
                'pk': i['St_Code'],
                'quantity': i['quantity'],
                'item_id': self.items[i['St_Class']],
                'user_id': i['us_id']
            }
            objs.append(UserItem(**kwargs))

        return len(UserItem.objects.bulk_create(objs))

    def user_stuff(self):
        self.cursor.execute('SELECT us_id, us_from, Stf_Class, '
                            'Stf_Comment, Stf_Time FROM UserStuff')
        objs = []
        for i in self.cursor.fetchall():
            kwargs = {
                'item_id': self.items[i['Stf_Class']],
                'user_from_id': i['us_from'],
                'user_id': i['us_id'],
                'comment': i['Stf_Comment'],
                'date': timezone.datetime.fromtimestamp(i['Stf_Time'])
            }
            objs.append(UserGift(**kwargs))
        return len(UserGift.objects.bulk_create(objs))

    def user_cart(self):
        objs = []
        self.cursor.execute('SELECT us_id, ClassName, Count(*) as quantity '
                            'FROM UserCart '
                            'GROUP BY us_id, ClassName')
        for i in self.cursor.fetchall():
            kwargs = {
                'user_id': i['us_id'],
                'item_id': self.items[i['ClassName']],
                'quantity': i['quantity']
            }
            objs.append(CartItem(**kwargs))
        return len(CartItem.objects.bulk_create(objs))

    def comments(self):
        self.cursor.execute('SELECT * FROM Comments2')
        comment_list = [self.cursor.fetchall()]
        for year in range(2010, 2017):
            self.cursor.execute('SELECT * FROM Comments2_%s' % year)
            comment_list.extend(self.cursor.fetchall())
        l = len(comment_list)
        objs = []
        created = 0
        for index, i in enumerate(comment_list):
            if index % 100 == 0 and index > 0 or index == l:
                a = Comment.objects.bulk_create(objs)
                created += len(a)
                objs = []
                print(created)

            self.cursor.execute('SELECT bobsoccer.blogs_article.id '
                                'from bobsoccer.blogs_article '
                                'where bobsoccer.blogs_article.id = %s' %
                                i['ClassItem'].split('.')[1])
            try:
                article = self.cursor.fetchall()[0]
            except IndexError:
                continue

            kwargs = {
                'pk': i['US_Code'],
                'content_type_id': 21,
                'parent_id': i['UC_Parent'],
                'user_id': i['us_id'],
                'object_id': article['id'],
                'text': i['US_Text'],
                'date': i['UC_Time'],
                'hide': True if i['UC_Status'] == 0 else False,
                'likes': i['UC_Vote']
            }
            objs.append(Comment(**kwargs))

    def forum(self):
        self.cursor.execute('SELECT * FROM forumThemes')
        objs = []
        for i in self.cursor.fetchall():
            self.cursor.execute('SELECT Top_Data from forumTopics where Top_Code = %s' % i['Top_Code'])
            try:
                text = self.cursor.fetchall()[0]['Top_Data']
            except AttributeError:
                continue

            kwargs = {
                'id': i['Th_Code'],
                'name': i['Subject'],
                'text': text,
                'created_at': timezone.datetime.fromtimestamp(i['Th_Time']),
                'user_id': i['us_id'],
                'show': i['Th_Status'],
                'answers': i['answers'],
                'last_comment': timezone.datetime.fromtimestamp(i['Th_lastTime'])
            }
            objs.append(Topic(**kwargs))
        topics = Topic.objects.bulk_create(objs)
        return len(topics)

    def forum_comments(self):
        self.cursor.execute('SELECT * FROM forumTopics '
                            'WHERE Top_Code > 0')
        created = 0
        for i in self.cursor.fetchall():
            if i['Top_Parent'] > 1:
                try:
                    parent = Comment.objects.get(
                        id=i['Top_Parent'], object_id=i['Th_Code'], content_type_id=21
                    )
                except Comment.DoesNotExist:
                    continue
            else:
                parent = None

            Comment.objects.create(
                id=i['Top_Code'], object_id=i['Th_Code'], content_type_id=21,
                text=i['Top_Data'], show=i['Top_Status'], user_id=i['us_id'],
                date=timezone.datetime.fromtimestamp(i['Top_Time']),
                parent=parent
            )
            created += 1
            if created % 500 == 0 and created > 1:
                print(created)

        return created


if __name__ == '__main__':
    export = Export()
    print('Выберите из: ')
    for method in export.methods:
        print('- %s' % method)

    print('')
    print('Exit для выхода')

    while True:
        command = raw_input('Введите команду: ')

        if command.lower() == 'exit':
            break
        else:
            try:
                func = export.__getattribute__(command)
                try:
                    result = func()
                    print('Успешно: %s' % result)
                except IntegrityError as e:
                    print(e)

            except AttributeError:
                print('Неизвестная команда')
