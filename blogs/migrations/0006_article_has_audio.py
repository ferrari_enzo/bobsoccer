# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-02-07 15:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0005_auto_20170207_1340'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='has_audio',
            field=models.BooleanField(default=False),
        ),
    ]
