# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-02-15 11:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0015_auto_20170213_0935'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('time', models.CharField(max_length=255)),
                ('file', models.CharField(max_length=255)),
                ('visible', models.BooleanField(default=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='broadcast',
            name='article',
        ),
        migrations.RemoveField(
            model_name='article',
            name='when_updated',
        ),
        migrations.DeleteModel(
            name='Broadcast',
        ),
        migrations.AddField(
            model_name='audio',
            name='article',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='blogs.Article'),
        ),
    ]
