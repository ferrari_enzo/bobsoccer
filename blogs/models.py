# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from urlparse import urlparse

import re
from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.models import Avg
from django.db.models import Q
from django.utils.translation import ugettext as _

from blogs.fields import AudioFileField
from soccer.models import Person

from django.contrib.contenttypes.fields import GenericRelation
from talks.models import Comment


class Tag(models.Model):
    name = models.CharField(max_length=255, unique=True)


class ArticleQuerySet(models.QuerySet):
    def tribune(self, *args, **kwargs):
        return self.filter(*args, **kwargs)

    def news(self, *args, **kwargs):
        kwargs['category'] = self.model.CAT.NEWS
        return self.filter(*args, **kwargs)

    def opinions(self, *args, **kwargs):
        # author's articles
        kwargs['source'] = ''
        return self.tribune(*args, **kwargs)

    def discuss(self, *args, **kwargs):
        kwargs['category'] = self.model.CAT.DISCUSS
        return self.filter(*args, **kwargs)

    def forecast(self, *args, **kwargs):
        kwargs['category'] = self.model.CAT.FORECAST
        return self.filter(*args, **kwargs)

    def interview(self, *args, **kwargs):
        kwargs['category'] = self.model.CAT.INTERVIEW
        return self.filter(*args, **kwargs)

    def experts(self, *args, **kwargs):
        kwargs['user__level__level'] = 50
        return self.filter(*args, **kwargs)

    def videos(self, *args, **kwargs):
        kwargs['has_video'] = True
        return self.filter(*args, **kwargs).order_by('-when_created')

    def audios(self, *args, **kwargs):
        kwargs['has_audio'] = True
        return self.filter(*args, **kwargs)


class Scenario(models.Model):
    name = models.CharField(max_length=255)


class Article(models.Model):
    class CAT:
        NEWS = 'news'
        INTERVIEW = 'interview'
        NOTE = 'note'
        DISCUSS = 'discuss'
        ANNOUNCE = 'announce'
        FORECAST = 'forecast'
        VIDEO = 'video'
        INITIATIVE = 'initiative'
        JUNIOR = 'junior'
        EDITION = 'edition'
        SPB = 'spb'
        _CHOICES = (
            (NEWS, _('Новость')),
            (INTERVIEW, _('Интервью')),
            (NOTE, _('Заметка')),
            (DISCUSS, _('Обсуждение')),
            (ANNOUNCE, _('Анонс')),
            (FORECAST, _('Блог конкурса прогнозов')),
            (VIDEO, _('Видео')),
            (INITIATIVE, _('Инициативы читателей')),
            (JUNIOR, _('Бобсоккер-юниор')),
            (EDITION, _('Редакционная хроника')),
            (SPB, _('Футбол Санкт-Петербурга'))
        )

    category = models.CharField(_('Категория'), max_length=20,
                                choices=CAT._CHOICES, default=CAT.NEWS,
                                db_index=True)
    title = models.CharField(_('Название'), max_length=255)
    announce = models.CharField(_('Анонс'), max_length=255, null=True, blank=True)
    text = models.TextField(_('Текст'))
    source = models.URLField(_('Источник'), blank=True, null=True)
    source_domain = models.CharField(max_length=255, verbose_name=_('Домен источника'), null=True, blank=True)

    person = models.ForeignKey(Person, verbose_name=_('Персона'),
                               related_name='articles', blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name=_('Пользователь'),
                             related_name='articles')
    when_created = models.DateTimeField(_('Дата'))
    date = models.DateTimeField(auto_now_add=True)
    has_video = models.BooleanField(default=False, verbose_name='Видеоматериалы')
    video_cover = models.CharField(max_length=100, null=True, blank=True)
    has_audio = models.BooleanField(default=False, verbose_name='Аудиоматериалы')
    as_announce = models.BooleanField(default=False, verbose_name='Использовать как анонс')
    ticker = models.BooleanField(default=False, verbose_name='В бегущую строку')
    not_publish = models.BooleanField(default=False, verbose_name='НЕ публиковать')

    scenario = models.ForeignKey(Scenario, related_name='articles', null=True, blank=True,
                                 verbose_name='Сценарий')

    tags = models.ManyToManyField(Tag, related_name='articles', blank=True)

    comments = GenericRelation(Comment, related_query_name='articles')
    total_views = models.PositiveIntegerField(default=0)

    objects = ArticleQuerySet.as_manager()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.source:
            parser = urlparse(self.source)
            self.source_domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parser)
        super(Article, self).save()

    class Meta:
        verbose_name = _('Статья')
        verbose_name_plural = _('Статьи')

    def __unicode__(self):
        return self.title

    def get_broadcast(self):
        try:
            return self.audio
        except ObjectDoesNotExist:
            return None

    def get_poll(self):
        try:
            return self.poll
        except ObjectDoesNotExist:
            return None

    def clean(self):
        if (self.category == self.CAT.INTERVIEW) ^ (self.person is not None):
            raise ValidationError(_('Персона должна быть указана в интервью'))

    def rating(self):
        return self.ratings.aggregate(rate=Avg('rate'))

    def get_rating(self):
        data = self.rating()
        data['count'] = self.ratings.count()
        data['users'] = self.ratings.values_list('user', flat=True)
        return data


class Audio(models.Model):
    article = models.OneToOneField(Article, related_name='audio')
    title = models.CharField(max_length=255)
    time = models.CharField(max_length=255)
    file = AudioFileField(upload_to='audios/')
    visible = models.BooleanField(default=True)


class ArticleRating(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='ratings')
    article = models.ForeignKey(Article, related_name='ratings')
    rate = models.PositiveSmallIntegerField(default=0)

    class Meta:
        unique_together = ('user', 'article')


class ArticleView(models.Model):
    ip = models.GenericIPAddressField()
    user_agent = models.TextField()
    article = models.ForeignKey(Article, related_name='views')


ArticleThrough = Article.tags.through
