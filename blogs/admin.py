from django.contrib import admin

from blogs.models import Article


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'when_created', 'category', 'person',
                    'source')
    list_filter = ('category', 'when_created')
    list_per_page = 5
