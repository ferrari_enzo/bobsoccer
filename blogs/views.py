# coding=utf-8
import json
from collections import defaultdict

from django.db.models import Max
from django.http import Http404
from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic import ListView

from blogs.models import Article, Tag, ArticleRating
from soccer.models import Person


class NewsListView(ListView):
    model = Article
    paginate_by = 20
    template_name = 'blogs/article_list.html'

    def get_queryset(self):
        return Article.objects.news().order_by('-when_created')

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        context['article_type'] = u'Новости'
        return context


class ExpertsListView(ListView):
    def get_queryset(self):
        return Article.objects.experts().order_by('-when_created')

    def get_context_data(self, **kwargs):
        context = super(ExpertsListView, self).get_context_data(**kwargs)
        context['article_type'] = u'Экспертное мнение'
        return context


class TribuneListView(ListView):
    paginate_by = 20
    model = Article
    template_name = 'blogs/article_list.html'

    def get_queryset(self):
        return Article.objects.tribune().order_by('-when_created')

    def get_context_data(self, **kwargs):
        context = super(TribuneListView, self).get_context_data(**kwargs)
        context['article_type'] = u'Трибуна'
        return context


class ForecastListView(ListView):
    paginate_by = 20
    model = Article
    template_name = 'blogs/article_list.html'

    def get_queryset(self):
        return Article.objects.forecast().order_by('-when_created')

    def get_context_data(self, **kwargs):
        context = super(ForecastListView, self).get_context_data(**kwargs)
        context['article_type'] = u'Блог конкурса прогнозов'
        return context


class DiscussListView(ListView):
    paginate_by = 20
    model = Article
    template_name = 'blogs/article_list.html'

    def get_queryset(self):
        return Article.objects.discuss().order_by('-when_created')

    def get_context_data(self, **kwargs):
        context = super(DiscussListView, self).get_context_data(**kwargs)
        context['article_type'] = u'Инициативы / онлайн обсуждения'
        return context


class NewsDetailView(DetailView):
    model = Article
    slug_field = 'pk'


class PersonsMixedListView(ListView):
    model = Person
    template_name = 'blogs/persons.html'

    def get_queryset(self):
        return Person.objects.annotate(last_comment=Max(
            'articles__when_created')).order_by('-last_comment')

    def get_context_data(self, **kwargs):
        context = super(PersonsMixedListView, self).get_context_data(**kwargs)
        persons = defaultdict(list)
        limits = {Person.TYPE.COACH: 4,
                  Person.TYPE.PLAYER: 8,
                  Person.TYPE.SPEC: 4}
        for person in self.object_list.iterator():
            if person.type not in limits:
                continue
            if len(persons[person.type]) < limits[person.type]:
                persons[person.type].append(person)
            else:
                del limits[person.type]
            if not limits:
                break

        context['persons'] = persons
        types = {'coaches': Person.objects.coaches(),
                 'players': Person.objects.players(),
                 'specs': Person.objects.specs()}
        context['types'] = types
        return context


class PersonsOneTypeListView(PersonsMixedListView):
    paginate_by = 4
    template_name = 'blogs/persons_one_type.html'

    def get_queryset(self, *args, **kwargs):
        q = self.kwargs.get('pr_type')
        return Person.objects.filter(type=q).order_by(
            '-articles__when_created')


class InterviewPersonDetailView(DetailView):
    model = Article
    template_name = 'blogs/interview_person.html'
    pk_url_kwarg = 'id'

    def get_queryset(self, *args, **kwargs):
        return Article.objects.interview()


class VideoListView(ListView):
    template_name = 'blogs/video.html'
    model = Article
    paginate_by = 10

    def get_queryset(self):
        return Article.objects.videos()

    def get_context_data(self, **kwargs):
        context = super(VideoListView, self).get_context_data(**kwargs)
        context['loop_times'] = range(1, 19)
        return context


class PersonPageDetailView(DetailView):
    model = Person
    template_name = 'blogs/person_page.html'
    pk_url_kwarg = 'id'

    def get_queryset(self):
        return Person.objects.filter(type=self.kwargs.get('pr_type'))

    def get_context_data(self, **kwargs):
        context = super(PersonPageDetailView, self).get_context_data(**kwargs)
        articles = self.object.articles.select_related('user')
        context['articles'] = articles
        return context


def tags_get(request):
    if request.method == 'POST' and request.is_ajax():
        q = request.POST.get('q')
        data = {
            'tags': list(Tag.objects.filter(name__istartswith=q).values_list('name', flat=True))
        }
        return JsonResponse(json.dumps(data))
    raise Http404()


def rating_view(request):
    if not (request.method == 'POST' and request.is_ajax()):
        raise Http404()

    try:
        article = Article.objects.get(pk=request.POST.get('article'))
    except Article.DoesNotExist:
        return HttpResponse('error')

    rate, created = ArticleRating.objects.get_or_create(user=request.user, article=article)
    rate.rate = request.POST.get('rate')
    rate.save()
    data = article.rating()
    data['count'] = article.ratings.count()

    return JsonResponse(json.dumps(data), safe=False)


class AudioListView(ListView):
    model = Article
    paginate_by = 20
    template_name = 'audio.html'

    def get_queryset(self):
        return Article.objects.audios().order_by('-when_created')
