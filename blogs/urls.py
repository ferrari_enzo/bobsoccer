from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^news/$', views.NewsListView.as_view(), name='news'),
    url(r'^news/(?P<pk>[0-9]+)/$', views.NewsDetailView.as_view(), name='news_detail'),

    url(r'^persons/$', views.PersonsMixedListView.as_view(), name='persons'),
    url(
        r'^persons/(?P<pr_type>\w+)/$',
        views.PersonsOneTypeListView.as_view(),
        name='persons_type_interviews'
    ),
    url(
        r'^interview/(?P<id>[0-9]+)/$',
        views.InterviewPersonDetailView.as_view(),
        name='interview_person_detail'
    ),
    url(
        r'^persons/(?P<pr_type>\w+)/(?P<id>[0-9]+)/$',
        views.PersonPageDetailView.as_view(),
        name='person_page'
    ),
    url(r'^video/$', views.VideoListView.as_view(), name='video'),

    url(r'^tags/get/$', views.tags_get, name='tags_get'),

    url(r'^tribune/$', views.TribuneListView.as_view(), name='tribune'),
    url(r'^experts/$', views.ExpertsListView.as_view(), name='experts'),
    url(r'^forecast/$', views.ForecastListView.as_view(), name='foreacst'),
    url(r'^initiative/$', views.DiscussListView.as_view(), name='discuss'),
    url(r'^rating/set/$', views.rating_view, name='rating_set'),

    url(r'^audio/$', views.AudioListView.as_view(), name='audio')
]
