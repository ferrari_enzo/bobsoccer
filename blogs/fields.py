# coding=utf-8
from django.db.models import FileField
from django import forms


class AudioFileField(FileField):
    content_types = ['audio/mpeg', 'audio/ogg', 'audio/vnd.wave',
                     'audio/x-ms-wma', 'audio/x-ms-max']

    def clean(self, value, model_instance):
        data = super(AudioFileField, self).clean(value, model_instance)
        f = data.file

        try:
            content_type = f.content_type
            if content_type not in self.content_types:
                raise forms.ValidationError(u'Неверный формат файла.')
        except AttributeError:
            pass

        return data
