from django import forms

from talks.models import Topic, Comment


class TopicCreateForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ('name', 'text')


class CommentCreateForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text', 'parent')
        widgets = {
            'parent': forms.HiddenInput
        }
