# -*- coding: utf-8 -*-


from __future__ import unicode_literals

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

from users.models import User

from django.utils.translation import ugettext as _

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation


# Create your models here.


class Topic(models.Model):
    name = models.CharField(_('Название'), max_length=30)
    user = models.ForeignKey(User, verbose_name=_('Пользователь'), related_name='topics')
    created_at = models.DateTimeField(verbose_name=_('Дата создания'), auto_now_add=True)
    comments = GenericRelation('Comment', related_query_name='topics')
    show = models.BooleanField(default=True)
    section = models.IntegerField(null=True, blank=True)
    last_comment = models.DateTimeField(null=True, blank=True)
    text = models.TextField()
    answers = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('Тема')
        verbose_name_plural = _('Темы')

    def __unicode__(self):
        return self.name


class Comment(MPTTModel):
    user = models.ForeignKey(User, related_name='comments')
    show = models.BooleanField(default=True)
    level = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    events = models.TextField(null=True, blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    likes = models.IntegerField(default=0)
    hide = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Коментарий')
        verbose_name_plural = _('Коментарии')

    def get_preview(self, length=50):
        if len(self.text) < 50:
            return self.text
        else:
            return '%s...' % self.text[:length]

    def __unicode__(self):
        return "%s: %s" % (self.user.username, self.text[:20])


class CommentLike(models.Model):
    actions = (
        ('like', 'Лайк'),
        ('dislike', 'Дизлайк')
    )
    comment = models.ForeignKey(Comment)
    user = models.ForeignKey(User, related_name='likes')
    action = models.CharField(max_length=8, choices=actions, default='like')
