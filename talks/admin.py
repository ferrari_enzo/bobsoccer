# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Topic, Comment

# Register your models here.


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('user', 'name', 'created_at')
    search_fields = ('topic_name',)


# @admin.register(Comment)
# class CommentAdmin(admin.ModelAdmin):
#     list_display = ('when_created', 'user', 'content_type', 'content_object', 'get_preview')
#     list_filter = ('content_type',)