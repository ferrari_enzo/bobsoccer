from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView

from talks.forms import TopicCreateForm, CommentCreateForm
from .models import Topic, Comment, CommentLike


class TopicListView(ListView):
    model = Topic
    paginate_by = 14
    template_name = 'forum.html'

    def get_queryset(self):
        return Topic.objects.all().order_by('-last_comment')


class TopicDetailView(DetailView):
    model = Topic
    template_name = 'forum_tema.html'
    context_object_name = 'topic'

    def get_context_data(self, **kwargs):
        context = super(TopicDetailView, self).get_context_data(**kwargs)
        context['comments'] = self.object.comments.all()
        context['forum'] = True
        return context

    def post(self, request, *args, **kwargs):
        form = CommentCreateForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.content_object = self.get_object()
            comment.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            return super(TopicDetailView, self).get(request, *args, **kwargs)


class TopicCreateView(CreateView):
    form_class = TopicCreateForm
    template_name = 'forum_create.html'
    success_url = '/forum/'

    def form_valid(self, form):
        topic = form.save(commit=False)
        topic.user = self.request.user
        topic.save()
        return super(TopicCreateView, self).form_valid(form)


def comment_like_view(request):
    if not (request.method == 'POST' and request.is_ajax()):
        raise Http404()

    try:
        comment = Comment.objects.get(pk=request.POST.get('comment'))
    except Comment.DoesNotExist:
        return HttpResponse('error')

    user_like, created = CommentLike.objects.get_or_create(
        user=request.user, comment=comment)

    action = request.POST.get('action')

    if not created and user_like.action == action:
            return HttpResponse(comment.likes)

    if not created:
        like = 2 if action == 'like' else -2
    else:
        like = 1 if action == 'like' else -1

    user_like.action = action
    user_like.save()
    comment.likes += like
    comment.save()

    return HttpResponse(comment.likes)