from django.shortcuts import render

# Create your views here.
from django.conf.urls import url
from .views import TopicListView, TopicDetailView, TopicCreateView, comment_like_view

urlpatterns = [
    url(r'^$', TopicListView.as_view(), name='forum'),
    url(r'^add/$', TopicCreateView.as_view(), name='forum_create'),
    url(r'^(?P<pk>[0-9]+)/$', TopicDetailView.as_view(), name='tema'),
    url(r'^comment/like/$', comment_like_view, name='comment_like')
]
