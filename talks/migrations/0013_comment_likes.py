# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-02-13 10:01
from __future__ import unicode_literals

from django.conf import settings

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('talks', '0012_auto_20170210_0554'),
    ]

    operations = [
        migrations.CreateModel(
            name='CommentLike',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.CharField(choices=[('like', '\u041b\u0430\u0439\u043a'),
                                                     ('dislike', '\u0414\u0438\u0437\u043b\u0430\u0439\u043a')],
                                            default='like', max_length=8)),
            ],
        ),
        migrations.AddField(
            model_name='comment',
            name='likes',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='commentlike',
            name='comment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='talks.Comment'),
        ),
        migrations.AddField(
            model_name='commentlike',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='likes',
                                    to=settings.AUTH_USER_MODEL),
        ),
    ]
