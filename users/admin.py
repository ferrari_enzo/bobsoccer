# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import Group

from users.forms import UserCreateAdmin
from .models import Country, City, Avatar, User, UserLevel, UserBan, UserItem

from django.utils.translation import ugettext as _

from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin


class CountryAdmin(admin.ModelAdmin):
    list_display = (u'id', u'name')
    search_fields = (u'name',)
admin.site.register(Country, CountryAdmin)


class CityAdmin(admin.ModelAdmin):
    list_display = (u'id', u'name')
    search_fields = (u'name',)
admin.site.register(City, CityAdmin)


class AvatarAdmin(admin.ModelAdmin):
    list_display = (u'id', u'pk', u'image')
    search_fields = (u'pk',)
admin.site.register(Avatar, AvatarAdmin)


class UserItemAdmin(admin.StackedInline):
    model = UserItem
    extra = 1


class UserLevelAdmin(admin.StackedInline):
    model = UserLevel
    fields = ('level', 'points', 'get_points_diff')
    readonly_fields = ('points', 'get_points_diff')


class UserBanAdmin(admin.StackedInline):
    model = UserBan
    fields = ('user', 'expired', 'active', 'last_ban', 'records_tag')
    readonly_fields = ('records_tag', )
    extra = 0


class UserAdmin(DefaultUserAdmin):
    inlines = [
        UserItemAdmin,
        UserLevelAdmin,
        UserBanAdmin
    ]

    add_form = UserCreateAdmin
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password', 'password2', 'email'),
        }),
    )
    ordering = ('-id', )
    filter_horizontal = []
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'birthdate', 'balance',
                                         'avatar', 'email', 'gender', 'country', 'city', 'team', 'skype')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'group')
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    list_display = (
        u'username',
        u'is_superuser',
        u'first_name',
        u'last_name',
        u'email',
        u'is_staff',
        u'is_active',
        u'skype',
        u'gender'
    )
    list_filter = (
        u'is_superuser',
        u'is_staff',
        u'is_active'
    )
    raw_id_fields = ('user_permissions',)


admin.site.unregister(Group)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)
    filter_horizontal = ('permissions',)
    qs_models = [
        'article',
        'album',
        'albumimage',
        'comment',
        'tournament',
        'match',
    ]

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permissions':
            qs = kwargs.get('queryset', db_field.remote_field.model.objects)
            # Avoid a major performance hit resolving permission names which
            # triggers a content_type load:
            kwargs['queryset'] = qs.select_related('content_type').filter(
                content_type__model__in=self.qs_models)
        return super(GroupAdmin, self).formfield_for_manytomany(
            db_field, request=request, **kwargs)


admin.site.register(User, UserAdmin)
