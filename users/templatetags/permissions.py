from django import template


register = template.Library()


@register.filter
def has_perm(user, perm):
    if user.is_anonymous():
        return False
    return user.has_group_perm(perm)
