# coding=utf-8
import re

from django import template

from blogs.models import ArticleRating

register = template.Library()


@register.inclusion_tag('messages/messages.html')
def show_messages(message):
    return {'message': message}


@register.inclusion_tag('messages/prev_message.html')
def show_prev(message):
    return {'message': message}


@register.inclusion_tag('messages/message.html')
def show_message(message, current=False):
    return {'message': message, 'current': current}


@register.inclusion_tag('messages/next_message.html')
def show_next(message):
    return {'message': message}


@register.filter
def create_range(value):
    return range(1, 32)


@register.filter
def get_user_level(level):
    return 'img/levels/level_%s.png' % level.level


@register.filter
def avg(value, total):
    return '%.1f' % round(value * 100. / total, 1)


@register.filter
def get_user_rating(user, article):
    try:
        return user.ratings.get(article=article).rate
    except ArticleRating.DoesNotExist:
        return u'Нет'


@register.filter
def voted(user, poll):
    if user.is_anonymous():
        return False

    if poll.votes.filter(user=user):
        return True
    else:
        return False


@register.filter
def get_video_cover(video_id):
    return 'https://img.youtube.com/vi/%s/hqdefault.jpg' % video_id
