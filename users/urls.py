from django.conf.urls import url
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(r'^items/$', views.user_items_view, name='user_items'),
    url(r'^items/get/$', views.get_user_items, name='items_get'),
    url(r'^items/send/$', views.gift_send, name='gift_send'),

    url(r'^friend/request/$', views.send_friend_request, name='friend_request'),

    url(r'^(?P<pk>[0-9]+)/$', views.ProfileDetailView.as_view(),
        name='profile'),
    url(r'^(?P<pk>[0-9]+)/update/$', views.ProfileUpdateView.as_view(),
        name='profile_update'),

    url(r'^(?P<pk>[0-9]+)/mail/$', views.ProfileMailView.as_view(),
        name='profile_mail'),
    url(r'^(?P<pk>[0-9]+)/messages/get/$', views.MessagesView.as_view(), name='messages_get'),
    url(r'^(?P<pk>[0-9]+)/messages/create/$', views.messages_create, name='messages_create'),

    url(r'^(?P<pk>[0-9]+)/messages/ignore/$', views.add_to_ignore, name='add_to_ignore'),

    url(r'^blog/create/$', views.article_create_view, name='article_create'),
    url(r'^(?P<pk>[0-9]+)/blog/$', views.article_list_view, name='article_list'),
    url(r'^blog/(?P<pk>[0-9]+)/update/$', views.article_update_view, name='article_update'),
    url(r'^(?P<pk>[0-9]+)/blog/(?P<article_id>[0-9]+)/$', views.article_detail_view, name='article_detail'),
    url(r'^blog/(?P<pk>[0-9]+)/delete/$', views.article_delete_view, name='article_delete'),
    url(r'^blog/(?P<pk>[0-9]+)/hide/$', views.article_hide_view, name='article_hide'),

    url(r'^(?P<pk>[0-9]+)/map/$', views.user_map, name='map'),

    url(r'^album/new/$', views.new_album, name='new_album'),

    url(r'^(?P<pk>[0-9]+)/photos/$', views.user_photos_all, name='photos_all'),

    url(r'^(?P<pk>[0-9]+)/album/gallery/$', views.gallery_photo, name='gallery_photo'),
    url(r'^(?P<pk>[0-9]+)/album/(?P<album>[0-9])/gallery/$', views.gallery_photo, name='gallery_album'),

    url(r'^(?P<pk>[0-9]+)/album/non_album/$', views.user_photo, name='photos'),
    url(r'^(?P<pk>[0-9]+)/album/(?P<album>[0-9])/$', views.user_photo, name='photos_album'),

    url(r'^(?P<pk>[0-9]+)/album/non_album/remove/$', views.album_remove, name='photos_remove'),
    url(r'^(?P<pk>[0-9]+)/album/(?P<album>[0-9])/remove/$', views.album_remove, name='album_remove'),

    url(r'^(?P<pk>[0-9]+)/album/non_album/hide/$', views.hide_album, name='photos_hide'),
    url(r'^(?P<pk>[0-9]+)/album/(?P<album>[0-9])/hide/$', views.hide_album, name='album_hide'),

    url(r'^(?P<pk>[0-9]+)/album/(?P<album>[0-9])/update/$', views.album_update, name='album_update'),

    url(r'^(?P<pk>[0-9]+)/coords/set/$', views.user_set_coords, name='set_coords'),

    url(r'^(?P<pk>[0-9]+)/photo/upload/$', views.user_upload_photo, name='upload_photos'),
    url(r'^(?P<pk>[0-9]+)/photo/(?P<album>[0-9])/upload/$', views.user_upload_photo, name='upload_photos_album'),
]
