# coding=utf-8
from __future__ import print_function

import re
from datetime import datetime

from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import formset_factory
from django.utils.safestring import mark_safe

from django.contrib.auth.forms import AuthenticationForm
from django import forms

from blogs.models import Article, Tag
from .models import User, Avatar, City, Country, Team, Message, Album
from captcha.fields import CaptchaField

from allauth.socialaccount.adapter import get_adapter
from allauth.socialaccount import app_settings


class SpecialSelect(forms.Select):
    def render(self, name, value, attrs=None, choices=()):
        simple_select = super(SpecialSelect, self).render(name, value, attrs)
        return '<div class="self_info">%s</div>' % simple_select


class SpecialSelectDateWidget(forms.SelectDateWidget):
    def create_select(self, name, field, value, val, choices, none_value):
        if 'id' in self.attrs:
            id_ = self.attrs['id']
        else:
            id_ = 'id_%s' % name
        if not self.is_required:
            choices.insert(0, none_value)
        local_attrs = self.build_attrs(id=field % id_)
        s = SpecialSelect(choices=choices)
        select_html = s.render(field % name, val, local_attrs)
        return select_html


class ImageChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return mark_safe("<img src='%s'/>" % obj.image.url)


class AuthForm(AuthenticationForm):
    def __init__(self, request=None, *args, **kwargs):
        super(AuthForm, self).__init__(request, *args, **kwargs)
        self.fields['username'].label = ''
        self.fields['username'].widget.attrs.update({'placeholder': 'Логин'})
        self.fields['password'].label = ''
        self.fields['password'].widget.attrs.update({'placeholder': 'Пароль'})


class SignupForm(forms.ModelForm):
    password1 = forms.CharField(label=u"Пароль",
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=u"Повторте пароль",
                                widget=forms.PasswordInput, )

    captcha = CaptchaField(label=u'Введите код', help_text=u'буквы вводятся в учетом регистра')
    rules = forms.BooleanField(label=u'Регистрация на портале означает согласие с правилами')
    avatar = ImageChoiceField(widget=forms.RadioSelect, queryset=Avatar.objects.all(), empty_label=None, required=False)

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email', 'first_name', 'skype',
                  'avatar', 'gender', 'birthdate', 'country', 'city', 'team', 'captcha', 'rules']

        labels = {
            'username': u'Логин',
            'email': u'Email',
            'captcha': u'Введите код',
            'skype': u'Skype Логин'
        }

        widgets = {
            'gender': SpecialSelect(),
            'country': SpecialSelect(),
            'city': SpecialSelect(),
            'team': SpecialSelect(),
            'birthdate': SpecialSelectDateWidget(years=range(datetime.now().year - 70, datetime.now().year - 17),
                                                 empty_label=None)
        }

        help_texts = {
            'username': u'псевдоним, будет указываться при всех ваших действиях на сайте',
            'email': u'вам будет отправлено письмо с кодом для подтверждения регистрации',
            'first_name': u'ваше настоящее имя (на сайте не отображается)',
            'skype': u'вы сможете задавать вопросы в прямом эфире (на сайте не отображается)',
            'team': u'выберете команду, которая вам наиболее симпотична',
            'captcha': u'буквы вводятся в учетом регистра',
        }

    def signup(self, request, user):
        user.first_name = self.cleaned_data['first_name']
        user.save()


class ProfileUpdateForm(forms.ModelForm):
    password_old = forms.CharField(label=u"Старый пароль",
                                   widget=forms.PasswordInput, required=False)
    password1 = forms.CharField(label=u"Новый пароль",
                                widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(label=u"Повторте пароль",
                                widget=forms.PasswordInput, required=False)
    avatar = ImageChoiceField(widget=forms.RadioSelect, queryset=Avatar.objects.all(), empty_label=None, required=False)

    class Meta:
        model = User
        fields = ['username', 'password_old', 'password1', 'password2', 'email', 'first_name', 'skype',
                  'avatar', 'gender', 'birthdate', 'country', 'city', 'team']

        labels = {
            'username': u'Логин',
            'email': u'Email',
            'captcha': u'Введите код',
            'skype': u'Skype Логин'
        }

        widgets = {
            'gender': SpecialSelect(),
            'country': SpecialSelect(),
            'city': SpecialSelect(),
            'team': SpecialSelect(),
            'birthdate': SpecialSelectDateWidget(years=range(datetime.now().year - 70, datetime.now().year - 17),
                                                 empty_label=None)
        }

        help_texts = {
            'username': u'псевдоним, будет указываться при всех ваших действиях на сайте',
            'email': u'вам будет отправлено письмо с кодом для подтверждения регистрации',
            'first_name': u'ваше настоящее имя (на сайте не отображается)',
            'skype': u'вы сможете задавать вопросы в прямом эфире (на сайте не отображается)',
            'team': u'выберете команду, которая вам наиболее симпотична',
            'captcha': u'буквы вводятся в учетом регистра',
        }

    def clean(self):
        password_old = self.cleaned_data.get('password_old')

        if not password_old:
            return self.cleaned_data

        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        user = User.objects.get(pk=self.instance.pk)

        if not user.check_password(password_old):
            self.add_error('password_old', 'Введите правильный пароль')
        elif password1 != password2:
            self.add_error('password1', 'Введенные пароли не совпадают')

        return self.cleaned_data


class SocialSignupForm(forms.Form):
    username = forms.CharField(label=u'Логин',
                               help_text=u'псевдоним, будет указываться при всех ваших действиях на сайте')
    email = forms.EmailField(label=u'Email', required=True,
                             help_text=u'вам будет отправлено письмо с кодом для подтверждения регистрации')
    first_name = forms.CharField(label=u'Имя', required=False,
                                 help_text=u'ваше настоящее имя (на сайте не отображается)')
    skype = forms.CharField(label=u'Skype', required=False,
                            help_text=u'вы сможете задавать вопросы в прямом эфире (на сайте не отображается)')
    avatar = ImageChoiceField(label=u'Образ', widget=forms.RadioSelect, queryset=Avatar.objects.all(), empty_label=None)

    birthdate = forms.DateField(label=u'День рождения', widget=SpecialSelectDateWidget(
        years=range(datetime.now().year - 70, datetime.now().year - 17)), required=False)
    country = forms.ModelChoiceField(widget=SpecialSelect, label=u'Страна', queryset=Country.objects.all(),
                                     required=False)
    city = forms.ModelChoiceField(widget=SpecialSelect, label=u'Город', queryset=City.objects.all(),
                                  required=False)
    team = forms.ModelChoiceField(widget=SpecialSelect, label=u'Команда', queryset=Team.objects.all(),
                                  help_text=u'выберете команду, которая вам наиболее симпотична',
                                  required=False)

    captcha = CaptchaField(label=u'Введите код', help_text=u'буквы вводятся в учетом регистра')
    rules = forms.BooleanField(label=u'Регистрация на портале означает согласие с правилами')

    def __init__(self, *args, **kwargs):
        self.sociallogin = kwargs.pop('sociallogin')
        initial = get_adapter().get_signup_form_initial_data(
            self.sociallogin)
        kwargs.update({
            'initial': initial,
            'email_required': kwargs.get('email_required',
                                         app_settings.EMAIL_REQUIRED)})

        # BaseSignupForm.__init__(self, *args, **kwargs)

    def save(self, request):
        adapter = get_adapter(request)
        user = adapter.save_user(request, self.sociallogin, form=self)
        self.custom_signup(request, user)
        return user

    def validate_unique_email(self, value):
        try:
            return super(SocialSignupForm, self).validate_unique_email(value)
        except forms.ValidationError:
            raise forms.ValidationError(
                get_adapter().error_messages['email_taken']
                % self.sociallogin.account.get_provider().name)


class MessageCreateForm(forms.ModelForm):
    user_to = forms.CharField()

    class Meta:
        model = Message
        fields = ('re', 'subject', 'text')

    def clean_user_to(self):
        user_to = self.cleaned_data.get('user_to')
        if not User.objects.filter(username=user_to).exists():
            self.add_error('user_to', u'Пользователя не найдено')
        return user_to


class ArticleForm(forms.ModelForm):
    tags = forms.CharField(required=False)
    poll_title = forms.CharField(max_length=255, label=u'Собственно сам вопрос', required=False)
    poll = forms.BooleanField(widget=forms.HiddenInput(), required=False)
    audio = forms.BooleanField(widget=forms.HiddenInput(), required=False)
    audio_title = forms.CharField(max_length=255, label=u'Заголовок', required=False)
    audio_start = forms.CharField(max_length=255, label=u'Время начала передачи', required=False)
    audio_file = forms.FileField(max_length=255, label=u'Файл записи', required=False)
    when_created = forms.DateTimeField(
        widget=forms.DateTimeInput(format='%d.%m.%Y %H:%M'),
        input_formats=['%d.%m.%Y %H:%M'],
        label=''
    )

    is_multiple = forms.TypedChoiceField(
        coerce=lambda x: x == 'True',
        choices=((False, u'Разрешен один вариант ответа'), (True, u'Разрешено несколько вариантов')),
        widget=forms.RadioSelect,
        label='',
        initial=False
    )

    obj_tags = []
    tags_changed = False

    class Meta:
        model = Article
        exclude = ('comments', 'user', 'tags', 'total_views', 'date', 'video_cover')
        widgets = {
            'text': CKEditorUploadingWidget(config_name='basic')
        }

    def save(self, commit=True):
        self.obj_tags = []
        if self.fields['tags'].has_changed(self.initial, self.data):
            tags = self.cleaned_data.get('tags')
            tags = re.sub(r'(?<=,)\s+', '', tags)

            for tag in tags.strip(',').split(','):
                if not tag:
                    continue

                tag, created = Tag.objects.get_or_create(name=tag)
                self.obj_tags.append(tag)
            self.tags_changed = True
        return super(ArticleForm, self).save(commit)

    def clean(self):
        if self.cleaned_data.get('audio'):
            if not self.cleaned_data.get('audio_title'):
                self.add_error('audio_title', 'Добавьте заголовок')

            if not self.cleaned_data.get('audio_start'):
                self.add_error('audio_start', 'Добавьте время начала передачи')

            if not self.cleaned_data.get('audio_file'):
                self.add_error('audio_file', 'Добавьте ссылку на файл')
        if self.cleaned_data.get('has_video') and self.cleaned_data.get('text'):
            video = self.video_exists()
            if not video:
                self.add_error('text',
                               'Вы должны добавить видео с youtube.com')
            else:
                self.cleaned_data['video_cover'] = video
        return self.cleaned_data

    def video_exists(self):
        regex = r'(?<=youtube\.com/embed/)[^\"]+'
        text = self.cleaned_data.get('text')
        videos = re.findall(regex, text)
        if videos:
            return videos[0]
        else:
            return None


class UserCreateAdmin(forms.ModelForm):
    password2 = forms.CharField(widget=forms.PasswordInput(),
                                label='Пароль еще раз')

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password2')
        widgets = {
            'password': forms.PasswordInput()
        }

    def __init__(self, *args, **kwargs):
        super(UserCreateAdmin, self).__init__(*args, **kwargs)
        self.fields['email'].required = True

    def save(self, commit=True):
        user = super(UserCreateAdmin, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()

        return user


class AlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = ('cover', 'name')


class AlbumImageForm(forms.Form):
    image = forms.ImageField(label='')
    title = forms.CharField(label='', required=False)
    text = forms.CharField(widget=forms.Textarea(), label='', required=False)


album_image_formset = formset_factory(AlbumImageForm, max_num=10)
