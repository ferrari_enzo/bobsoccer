window.onload = function () {
    var defaultLatLng = {lat: 54.755826, lng: 37.6173};
    var userLatLng = {lat: latUser, lng: lngUser};

    if (userLatLng != null)
        createMapAndReestablishMarker(userLatLng);
    else
        createMapAndInitMarker(defaultLatLng);
};

// function createMapAndInitMarker(defaultLatLng){
//   var map = new google.maps.Map(document.getElementById('map'), {
//     zoom: 7,
//     center: defaultLatLng
//   });
//
//   var clickMapListener = google.maps.event.addListener(map, 'click', function (event) {
//     var marker = addMarker(map, event.latLng);
//
//     var infowindow = createInfoWindow(map, marker);
//
//     google.maps.event.addListener(marker, "dragstart", function() {
//       infowindow.close();
//     });
//
//     google.maps.event.addListener(marker, "dragend", function() {
//       infowindow = createInfoWindow(map, marker);
//     });
//
//     google.maps.event.removeListener(clickMapListener);
//   });
// }

function createMapAndReestablishMarker(userLatLng) {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoomUser,
        center: userLatLng
    });

    globalZomm = map;

    var marker = addMarker(map, userLatLng);
    if (drag == true) {
        var infowindow = createInfoWindow(map, marker);
    }
    google.maps.event.addListener(marker, "dragstart", function () {
        infowindow.close();
    });

    google.maps.event.addListener(marker, "dragend", function (newCoord) {
        userCoord = newCoord.latLng;
        infowindow = createInfoWindow(map, marker);
    });
}

function createMap(latLng) {

}

function addMarker(map, latLng) {
    return new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: drag,
        icon: "http://www.bobsoccer.ru/img/marker.png"
    });
}

function createInfoWindow(map, marker) {
    var contentString = '<div id="mapInfoWindow">'
        + '<div class="form-group"><textarea class="form-control"></textarea></div>'
        + '<button class="btn btn-primary" onclick="setInfoWindowText();">Ok</button>'
        + '</div>';

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    infowindow.open(map, marker);

    return infowindow;
}

function setInfoWindowText() {
    var mapInfoWindowTag = document.querySelector('#mapInfoWindow');
    var status = document.querySelector('#mapInfoWindow textarea').value;
    mapInfoWindowTag.innerHTML = status;
    zoomUser = globalZomm.getZoom();
    sentStatusToServer(status, urlUser);
}

function sentStatusToServer(status, urlUser) {
    $.ajax({
        type: "POST",
        url: urlUser,
        data: {'title': status, "lat": userCoord.lat(), "lng": userCoord.lng(), "zoom": zoomUser}
    });
}