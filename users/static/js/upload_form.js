function validateSize(fileInput, size) {
    var fileObj, oSize;
    if (typeof ActiveXObject == "function") { // IE
        fileObj = (new ActiveXObject("Scripting.FileSystemObject")).getFile(fileInput.value);
    } else {
        fileObj = fileInput.files[0];
    }

    oSize = fileObj.size; // Size returned in bytes.
    if (oSize > size * 1024 * 1024) {
        return false
    }
    return true;
}
function appendPhotoForm(num) {
    $('.upload>.row').append('<div class="col-md-6 col-xs-12">' +
        '<div class="col-xs-12 padding-off upload-photo">' +
        '<span class="count-upload">' + parseInt(num+1) + '</span>' +
        '<input class="img_file" accept="image/*" type="file" name="form-' + num + '-image">' +
        '<p>jpg, gif, png не более 1.5 Mb</p>' +
        '<div class="form-group"><input class="form-control" type="text" name="form-' + num + '-title" placeholder="Краткое название фотографии"></div>' +
        '<div class="form-group"><textarea class="form-control" name="form-' + num + '-text" rows="4" placeholder="Опишите что происходит на фотографии, кто присутствует"></textarea></div>' +
        '</div>' +
        '</div>');
    $('input[name="form-TOTAL_FORMS"]').val(num + 1);
}
$(document)
    .on("change", ".img_file", function () {
        if (!validateSize(this, 1.5)) {
            $.notify({
                message: 'Размер файла превышает 1.5 MB'
            }, {
                type: 'danger'
            });
            $(this).val("")

        } else if (!$(this).hasClass('img_file_nonactive')) {
            $(this).addClass("img_file_nonactive");
            var countPhoto = $('.upload-photo').length;
            if (countPhoto < 11) {
                appendPhotoForm(countPhoto);
            }
        }
    })
    .ready(function () {
        appendPhotoForm(0)
    });