$(document).ready(function () {
    $('.bxslider').bxSlider({
        minSlides: 2,
        maxSlides: 4,
        auto: true,
        moveSlides: 1,
        pause: 3000,
        slideWidth: 280,
        slideMargin: 10
    });

    $('#myTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show')
    });

    $(".vhod").click(function () {
        if ($(".login").css("display") === "none") {
            $(".login").css("display", "inline");
            $(".forgot_pw").css("display", "inline");
        } else {
            $(".login").css("display", "none");
            $(".forgot_pw").css("display", "none");
        }
        ;
    });
    $(".for_search").click(function () {
        if ($(".input_for_search").css("display") === "none") {
            $(".input_for_search").css("display", "inline");
        } else {
            $(".input_for_search").css("display", "none");
        }
        ;
    });
});


/* code for new questions */
$(document).ready(function () {
    /* x=кол-во вопросов в опросе*/
    var pollsArray = 0;
    var x = 10;
    var i = $(".all-var-quest .number-quest").length;
    var y = x - i;
    $(document).on("click", ".questions-btn", function () {
        $('.questions-add').slideToggle(1500);
        if ($(this).attr('id') == 'activated-quest') {
            $(this).removeAttr('id');
            $(this).text('Добавить опрос');
            $("#id_poll").attr("value", "False")

        }
        else {
            $(this).attr("id", "activated-quest");
            $(this).text('Убрать опрос');
            $("#id_poll").attr("value", "True");
        }
    });

    $(document).on("click", ".btn-transl", function () {
        $('.add-transl').slideToggle(1500);
        if ($(this).attr('id') == 'activated-transl') {
            $(this).removeAttr('id');
            $(this).text('Добавить передачу');
            $("#id_audio").attr("value", "False");
        }
        else {
            $(this).attr("id", "activated-transl");
            $(this).text('Убрать передачу');
            $("#id_audio").attr("value", "True");
        }
    });

    $('.questions-add').append('<a id="add-quest">Добавить вопрос(еще ' + y + ')</a>');
    $(document).on("click", "#add-quest", function () {
        console.log('qwdqwd');
        if (i < x) {
            $('.all-var-quest').append('<div class="form-group number-quest">' +
                '<input placeholder="Текст варианта" class="form-control quest-input">' +
                '<a class="dell-quest new-dell-quest">×</a></div>');

            $('input.quest-input').last().attr('name', 'form-' + i + '-text');
            i++;
            y = x - i;
            $('#add-quest').text('Добавить вопрос(еще ' + y + ')');
            if (i == x) {
                $('#add-quest').remove()
            }
            var total_form = $('#id_form-TOTAL_FORMS');
            total_form.val(parseInt(total_form.val()) + 1)
        }
    });
    $(document).on("click", ".dell-quest", function () {
        if (i == x) {
            y = x - i;
            $('.questions-add').append('<a id="add-quest">Добавить вопрос(еще ' + y + ')</a>');
        }
        $(this).parent(".number-quest").remove();
        i--;
        y = x - i;
        $('#add-quest').text('Добавить вопрос(еще ' + y + ')');
        var total_form = $('#id_form-TOTAL_FORMS');
        total_form.val(parseInt(total_form.val()) - 1);

        var pollsNumber = $(".all-var-quest .number-quest").length;
        pollsArray = $(".number-quest input");
        $('#id_form-TOTAL_FORMS').attr("value", pollsNumber);
        for (var u = 0; u < pollsNumber; u++) {
            $(pollsArray).eq(u).attr("name", 'form-' + u + '-text')
        }
    })
        .on('click', '.article_hide', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: $(this).attr('href'),
                success: function (data) {
                    console.log(data)
                },
                error: function (error) {
                    console.log(error)
                }
            })
        })
});

