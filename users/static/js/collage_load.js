$(window).load(function () {
        $('.Collage').collagePlus(
       {
           'targetHeight'    : 220,
           'allowPartialLastRow'       : false,
           'effect' : "effect-5" 
       }
   );
        var resizeTimer = null;
		$(window).bind('resize', function() {
		    // hide all the images until we resize them
		    // set the element you are scaling i.e. the first child nodes of ```.Collage``` to opacity 0
		    $('.Collage .Image_Wrapper').css("opacity", 0);
		    // set a timer to re-apply the plugin
		    if (resizeTimer) clearTimeout(resizeTimer);
		    resizeTimer = setTimeout(Collage, 200);
		});
 });
