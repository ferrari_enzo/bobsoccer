	$( document ).ready( function(){
	var selectorMailsLink = ".navigation a.mails" ;
	var selectorContactsLink = ".navigation a.contacts" ;	
	var selectorMailsGroup = ".navigation .for_mails" ;
	var selectorContactsGroup = ".navigation .for_contacts" ;
	var selectorSecondLevelLinksForMails = ".navigation .for_mails a";	
	var selectorSecondLevelLinksForContacts = ".navigation .for_contacts a"
	
	$( selectorMailsLink ).click( function(){
		if(!$(this).hasClass("selected")){
			$(this).addClass("selected");
			$( selectorContactsLink ).removeClass("selected");
			$( selectorContactsGroup ).attr("style", "display:none;");
			$( selectorMailsGroup ).attr("style", "display:block;");
			$( selectorSecondLevelLinksForMails + ", " + selectorSecondLevelLinksForContacts ).removeClass("selected");
			$( selectorSecondLevelLinksForMails + ":first-child" ).addClass("selected");
		}		
	});
	$( selectorContactsLink ).click( function(){
		if(!$(this).hasClass("selected")){
			$(this).addClass("selected");
			$( selectorMailsLink ).removeClass("selected");
			$( selectorContactsGroup ).attr("style", "display:block;");
			$( selectorMailsGroup ).attr("style", "display:none;");
			$( selectorSecondLevelLinksForMails + ", " + selectorSecondLevelLinksForContacts ).removeClass("selected");
			$( selectorSecondLevelLinksForContacts + ":first-child" ).addClass("selected");
		}		
	});
	$( selectorSecondLevelLinksForMails + ", " + selectorSecondLevelLinksForContacts ).click( function(){
		if(!$(this).hasClass("selected")){
			$( selectorSecondLevelLinksForMails + ", " + selectorSecondLevelLinksForContacts ).removeClass("selected");
			$(this).addClass("selected");
		}
	});	
});