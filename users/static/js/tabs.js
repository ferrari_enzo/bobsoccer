/**
 * Created by sergejandreev on 21.11.16.
 */
(function ($) {
    $.fn.ajaxTabpanel = function () {

        function ajaxTabRequest(tab, url) {
            $.ajax({
                url: url,
                success: function (text) {
                    tab.html(text);
                    tab.attr("data-full", "true")
                },
                error: function () {
                    alert('Извините что-то пошло не так');
                }
            });
        }

        $tab_block = $(this).find(".ajax-tabs");

        $active_tab_a = $tab_block.find("li.active").find("a[role='tab']");
        $active_tab = $($active_tab_a.attr("href"));

        ajaxTabRequest($active_tab, $active_tab_a.attr("data-ajax-url"));

        $tab_block.find("a[role='tab']").on("click", function () {

            var tab_id = $(this).attr("href");
            var url = $(this).attr("data-ajax-url");

            $tab = $(tab_id);
            if(!$tab.attr("data-full")){
                ajaxTabRequest($tab, url)
            }
        })
    }
})(jQuery);