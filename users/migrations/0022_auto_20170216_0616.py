# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-02-16 06:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0021_auto_20170216_0559'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='day',
        ),
        migrations.RemoveField(
            model_name='user',
            name='month',
        ),
        migrations.RemoveField(
            model_name='user',
            name='year',
        ),
        migrations.AddField(
            model_name='user',
            name='birthdate',
            field=models.DateTimeField(blank=True, null=True, verbose_name='\u0414\u0435\u043d\u044c \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f'),
        ),
    ]
