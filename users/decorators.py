from django.http import Http404
from django.shortcuts import get_object_or_404

from blogs.models import ArticleView, Article
from users.models import User


def for_current_user(func):
    def wrap(request, *args, **kwargs):
        if request.user.pk != int(kwargs.get('pk', 0)):
            raise Http404()
        return func(request, *args, **kwargs)
    return wrap


def for_current_user_or_perm(perm, model=None):
    def _method_wrapper(func):
        def _wrapper(request, *args, **kwargs):
            pk = int(kwargs.get('pk', 0))

            if model:
                obj = get_object_or_404(model, pk=pk)
                kwargs['obj'] = obj
                pk = obj.user.pk

            if request.user.pk == pk or request.user.has_group_perm(perm):
                return func(request, *args, **kwargs)

            raise Http404()
        return _wrapper
    return _method_wrapper


def views_count(func):
    def wrap(request, pk, article_id, *args, **kwargs):
        user = get_object_or_404(User, pk=pk)
        article = get_object_or_404(Article, user=user, id=article_id)
        ip = request.META.get('REMOTE_ADDR')
        user_agent = request.META.get('HTTP_USER_AGENT')

        view, created = ArticleView.objects.get_or_create(
            ip=ip, user_agent=user_agent, article=article)

        if created:
            article.total_views += 1
            article.save()
        return func(request, user, article)

    return wrap
