# coding=utf-8

from allauth.account.views import SignupView
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import Http404
from django.http.response import JsonResponse, HttpResponse, \
    HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.html import escape
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView

from blogs.models import Article, Audio
from bobsoccer.decorators import ajax_post_only
from common.utils import delete_cache
from polls.forms import poll_formset
from polls.models import Poll, PollVariant
from shop.models import Item
from talks.forms import CommentCreateForm
from .decorators import for_current_user_or_perm, views_count, for_current_user
from .forms import ProfileUpdateForm, MessageCreateForm, ArticleForm, album_image_formset, AlbumForm
from .models import User, Message, UserLocation, AlbumImage, UserGift, UserItem, FriendRequest, Album


class MySignupView(SignupView):
    success_url = '/users/success_signup/'


class ProfileDetailView(DetailView):
    model = User
    template_name = 'lk.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(**kwargs)
        context['user'] = self.get_object()
        return context


class ProfileUpdateView(UpdateView):
    model = User
    template_name = 'account/signup.html'
    form_class = ProfileUpdateForm

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        context['profile_update'] = True
        return context

    def form_valid(self, form):
        user = form.save(commit=False)
        password = form.cleaned_data.get('password1')
        if password:
            user.set_password(password)
        user.save()
        self.success_url = reverse('accounts:profile', kwargs={'pk': user.pk})
        return super(ProfileUpdateView, self).form_valid(form)


class ProfileMailView(View):
    template_name = 'messages/base.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class MessagesView(View):
    names = {
        'inbox': u'Входящие',
        'spam': u'Спам',
        'sent': u'Отправленные',
        'friends': u'Друзья',
        'ignores': u'Игнорируемые',
        'contacts': u'Контакты'
    }
    template_name = ''
    context = {}

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            block = request.POST.get('block', 'messages')
            func = self.__getattribute__(block)
            if func:
                func(request)

            return render(request, self.template_name, self.context)

        raise Http404()

    def messages(self, request):
        messages_type = request.POST.get('type', 'inbox')
        func = getattr(Message, 'get_%s_messages' % messages_type)

        self.context.update({
            'table_name': self.names[messages_type],
            'messages': func(request.user) if func else []
        })
        self.template_name = 'messages/list.html'

    def contacts(self, request):
        contacts_type = request.POST.get('type', 'contacts')
        func = getattr(request.user, 'get_%s' % contacts_type)
        contacts = func() if func() else []

        self.context.update({
            'table_name': self.names[contacts_type],
            'contacts': contacts
        })
        self.template_name = 'contacts/list.html'

    def write_message(self, request):
        re = request.POST.get('msg')
        if re:
            try:
                re = Message.objects.get(pk=re)
            except Message.DoesNotExist:
                pass

        self.context.update({
            'table_name': u'Новое сообщение',
            're': re
        })
        self.template_name = 'messages/create.html'

    def settings(self, request):
        self.context.update({
            'table_name': u'Настройки сообщений'
        })
        self.template_name = 'messages/settings.html'

    def message_detail(self, request):
        msg = request.POST.get('msg')
        ignore = False
        try:
            message = Message.objects.get(pk=msg)

            if message.user_from in request.user.ignore.all():
                ignore = True

        except Message.DoesNotExist:
            message = None

        self.context.update({
            'object': message,
            'ignore': ignore
        })
        self.template_name = 'messages/detail.html'


@login_required
@ajax_post_only
def messages_create(request, *args, **kwargs):
    form = MessageCreateForm(request.POST)
    response = {'success': False}
    if form.is_valid():
        message = form.save(commit=False)
        user_to = User.objects.get(username=form.cleaned_data['user_to'])
        message.user_from = request.user
        message.user_to = user_to
        message.save()
        response['success'] = True

    if form.errors:
        response.update(form.errors)
    return JsonResponse(response)


@ajax_post_only
def add_to_ignore(request, *args, **kwargs):
    action = request.POST.get('action', 'add')
    success = True

    try:
        user = User.objects.get(pk=request.POST.get('user'))
        if action == 'add':
            request.user.ignore.add(user)
        elif action == 'remove':
            request.user.ignore.remove(user)

    except User.DoesNotExist:
        success = False

    return JsonResponse({'success': success})


@ajax_post_only
def contacts_view(request, pk):
    return render(request, 'contacts/list.html', {
        'contacts': request.user.friends.all()})


@login_required
def article_create_view(request):
    form = ArticleForm(request.POST or None, initial={
        'when_created': timezone.now()
    })

    formset = poll_formset(request.POST or None)

    if form.is_valid():
        error = False
        article = form.save(commit=False)
        article.user = request.user
        article.video_cover = form.cleaned_data.get('video_cover')
        article.save()

        poll_title = form.cleaned_data.get('poll_title')
        poll = form.cleaned_data.get('poll')

        if poll:
            formset = poll_formset(request.POST)
            if formset.is_valid():

                poll, created = Poll.objects.update_or_create(
                    article=article, defaults={
                        'title': poll_title,
                        'is_multivariant': form.cleaned_data.get('is_multiple'),
                    }
                )

                variants = []
                for f in formset:
                    text = f.cleaned_data.get('text')
                    if not text:
                        continue

                    variants.append(PollVariant(text=text, poll=poll))

                if not poll_title and variants:
                    error = True
                    form.add_error('poll_title', u'Введите вопрос')

                elif len(variants) < 2:
                    error = True
                    form.add_error(None, u'Добавьте ответов к опросу')

                else:
                    PollVariant.objects.bulk_create(variants)
            else:
                error = True

        audio = form.cleaned_data.get('audio')
        if audio:
            data = {
                'article_id': article.id,
                'title': form.cleaned_data.get('audio_title'),
                'time': form.cleaned_data.get('audio_start'),
                'file': form.cleaned_data.get('audio_file')
            }
            Audio.objects.create(**data)

        if not error:
            delete_cache('articles_block')
            messages.add_message(request, messages.SUCCESS, "Статья создана!")
            return HttpResponseRedirect(reverse('accounts:profile',
                                                kwargs={'pk': request.user.pk}))
    return render(request, 'blogs/create.html', {
        'form': form, 'formset': formset
    })


def article_list_view(request, pk):
    user = get_object_or_404(User, pk=pk)
    paginator = Paginator(user.articles.all(), 25)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)

    context = {
        'articles': articles,
        'user': user
    }
    if paginator.num_pages > 1:
        context['is_paginated'] = True

    return render(request, 'blogs/list.html', context)


@login_required
@for_current_user_or_perm('blogs.change_article', model=Article)
def article_update_view(request, obj, **kwargs):
    tags = ','.join(list(obj.tags.values_list('name', flat=True)))
    initial = {
        'tags': tags,
    }

    poll_initial = {}
    if obj.get_poll():
        initial.update({
            'poll_title': obj.poll.title,
            'is_multiple': obj.poll.is_multivariant,
            'poll': True
        })
        poll_initial = obj.poll.options.values('text')

    if obj.get_broadcast():
        initial.update({
            'audio_title': obj.audio.title,
            'audio_start': obj.audio.time,
            'audio_file': obj.audio.file,
            'audio': True
        })

    form = ArticleForm(
        instance=obj,
        initial=initial,
        data=request.POST or None,
        files=request.FILES or None
    )
    print(form.is_valid())
    print(request.FILES)
    print(form.cleaned_data)
    formset = poll_formset(initial=poll_initial, data=request.POST or None)
    if form.is_valid():
        error = False
        article = form.save(commit=False)
        article.video_cover = form.cleaned_data.get('video_cover')

        poll_title = form.cleaned_data.get('poll_title')
        poll = form.cleaned_data.get('poll')

        if poll:
            if formset.is_valid():

                poll, created = Poll.objects.update_or_create(
                    article=article, defaults={
                        'title': poll_title,
                        'is_multivariant': form.cleaned_data.get('is_multiple'),
                    }
                )

                variants = []
                for f in formset:
                    text = f.cleaned_data.get('text')

                    if not text:
                        continue

                    variants.append(PollVariant(text=text, poll=poll))

                if not poll_title and variants:
                    error = True
                    form.add_error('poll_title', u'Введите вопрос')

                elif len(variants) < 2:
                    error = True
                    form.add_error(None, u'Добавьте ответов к опросу')

                else:
                    poll.options.all().delete()
                    PollVariant.objects.bulk_create(variants)
            else:
                error = True

        audio = form.cleaned_data.get('audio')
        if audio:
            data = {
                'article_id': article.id,
                'title': form.cleaned_data.get('audio_title'),
                'time': form.cleaned_data.get('audio_start'),
                'file': form.cleaned_data.get('audio_file')
            }
            Audio.objects.create(**data)
        else:
            try:
                article.audio.delete()
                article.has_audio = False
                article.save()
            except Audio.DoesNotExist:
                pass

        article.save()
        article.tags.clear()

        if form.obj_tags:
            article.tags.add(*form.obj_tags)

        if not error:
            delete_cache('articles_block')
            messages.add_message(request, messages.SUCCESS, "Статья обновлена!")
            return HttpResponseRedirect(reverse('accounts:article_list',
                                                kwargs={'pk': obj.user.id}))

    return render(request, 'blogs/create.html', {
        'form': form, 'article': obj,
        'formset': formset
    })


@views_count
def article_detail_view(request, user, article):
    context = {
        'article': article,
        'comments': article.comments.all(),
        'user': user
    }

    if request.method == 'POST':
        form = CommentCreateForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.content_object = article
            comment.user = request.user
            comment.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return render(request, 'blogs/detail.html', context)


def user_photo(request, pk, album=None):
    user = get_object_or_404(User, pk=pk)
    if album:
        album = get_object_or_404(Album, pk=album)

    if album:
        images = album.images.all()
    else:
        images = user.images.filter(album__isnull=True)

    context = {
        'user': user,
        'album': album,
        'images': images
    }
    return render(request, 'photo/user_photo.html', context)


def user_map(request, pk):
    user = get_object_or_404(User, pk=pk)
    context = {
        'user': user
    }
    try:
        location = user.location
        context.update({
            'lng': location.lng,
            'lat': location.lat,
            'title': location.title
        })
    except UserLocation.DoesNotExist:
        pass

    return render(request, 'map.html', context)


@ajax_post_only
def user_set_coords(request, pk):
    if request.user.pk == int(pk):
        lat = request.POST.get('lat')
        lng = request.POST.get('lng')
        title = request.POST.get('title')
        zoom = request.POST.get('zoom')
        coords, created = UserLocation.objects.get_or_create(
            user_id=pk, lat=lat, lng=lng, title=title, zoom=zoom
        )
        return HttpResponse('ok')


def user_items_view(request):
    return render(request, 'items/list.html',
                  {'items': request.user.items.all()})


@csrf_exempt
def ckeditor_image_upload_view(request):
    image = AlbumImage.objects.create(
        image=request.FILES['upload'], user=request.user)
    ck_func_num = escape(request.GET.get('CKEditorFuncNum'))
    return HttpResponse("""
    <script type='text/javascript'>
        window.parent.CKEDITOR.tools.callFunction({0}, '{1}');
    </script>""".format(ck_func_num, image.image.url))


@csrf_exempt
def ckeditor_image_browse_view(request):
    context = {
        'images': request.user.images.all()
    }
    return render(request, 'ckeditor/browse.html', context)


@ajax_post_only
def send_friend_request(request):
    try:
        user = User.objects.get(pk=request.POST.get('friend'))
    except User.DoesNotExist:
        return HttpResponse('error')

    if user in request.user.get_friends():
        return HttpResponse('exists')

    try:
        FriendRequest.objects.get(
            user_from=request.user, user_to=user)

    except FriendRequest.DoesNotExist:
        try:
            req = FriendRequest.objects.get(
                user_from=user, user_to=request.user)
            user.friends.add(request.user)
            request.user.friends.add(user)
            user.save()
            request.user.save()
            req.delete()
        except FriendRequest.DoesNotExist:
            FriendRequest.objects.create(user_from=request.user, user_to=user)

    return HttpResponse('success')


@ajax_post_only
def gift_send(request):
    comment = request.POST.get('comment')
    if not comment:
        return JsonResponse({
            'status': 'error',
            'comment': u'Надо написать комментарий'
        })
    user_to = request.POST.get('user_to')
    if not user_to:
        return JsonResponse({
            'status': 'error',
            'user_to': u'Добавьте получателя'
        })

    try:
        user_item = UserItem.objects.get(
            user=request.user, item_id=request.POST.get('item'))
        item = user_item.item
    except UserItem.DoesNotExist:
        return JsonResponse({
            'status': 'error',
            'item': u'Предмета не существует'
        })

    UserGift.objects.create(
        user_from=request.user, user_id=user_to,
        comment=comment, item=item
    )
    if user_item.quantity == 1:
        user_item.delete()
    else:
        user_item.quantity -= 1
        user_item.save()

    return JsonResponse({
        'status': 'success',
        'item': {
            'name': item.name,
            'image': item.image.url,
            'user': user_item.user.username
        }
    })


@ajax_post_only
def get_user_items(request):
    data = Item.objects.dicts(users__user=request.user)
    return JsonResponse(data, safe=False)


@for_current_user_or_perm('users.change_albumimage')
def user_upload_photo(request, pk, album=None):
    if album:
        album = get_object_or_404(Album, pk=album)

    formset = album_image_formset(request.POST or None, request.FILES or None)
    images = []

    if formset.is_valid():
        for form in formset:
            data = form.cleaned_data
            if data:
                data['album'] = album
                data['user'] = request.user
                images.append(AlbumImage(**data))
        AlbumImage.objects.bulk_create(images)
        messages.add_message(request, messages.SUCCESS,
                             'Фотографии успешно добавлены')
        if album:
            return redirect('accounts:photos_album', pk, album.pk)
        else:
            return redirect('accounts:photos', pk)
    context = {
        'user': get_object_or_404(User, pk=pk),
        'formset': formset,
        'album': album
    }
    return render(request, 'photo/user_upload_photo.html', context)


def gallery_photo(request, pk, album=None):
    user = get_object_or_404(User, pk=pk)

    if album:
        album = get_object_or_404(Album, pk=album, user=user)

    if album:
        images = album.images.all()
    else:
        images = user.images.filter(album__isnull=True)

    context = {
        'user': user,
        'images': images,
        'current': request.GET.get('image')
    }
    return render(request, 'photo/gallery_photo.html', context)


def new_album(request):
    form = AlbumForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        album = form.save(commit=False)
        album.user = request.user
        album.save()
        return redirect(reverse('accounts:photos_album', args=[
            request.user.pk, album.pk]))
    context = {
        'user': request.user,
        'form': form
    }
    return render(request, 'photo/new_album_photo.html', context)


@for_current_user_or_perm('users.change_albumimage')
def album_update(request, pk, album):
    album = get_object_or_404(Album, user_id=pk, pk=album)
    form = AlbumForm(request.POST or None, request.FILES or None,
                     instance=album)
    if form.is_valid():
        album = form.save()
        return redirect(reverse('accounts:photos_album', args=[
            request.user.pk, album.pk]))

    context = {
        'user': get_object_or_404(User, pk=pk),
        'form': form
    }
    return render(request, 'photo/new_album_photo.html', context)


@for_current_user_or_perm('users.delete_album')
def album_remove(request, pk, album=None):
    user = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        if album:
            try:
                album = Album.objects.geT(user=user, pk=album)
                album.delete()
            except Album.DoesNotExist:
                return redirect('accounts:photos_all')
        else:
            user.images.filter(album__isnull=True).delete()
    return redirect('accounts:photos_all', user.pk)


def user_photos_all(request, pk):
    user = get_object_or_404(User, pk=pk)
    context = {
        'user': user,
        'images': user.images.all(),
        'album': 'all'
    }
    return render(request, 'photo/user_photo.html', context)


@for_current_user_or_perm('users.change_albumimage')
def hide_album(request, pk, album=None):
    user = get_object_or_404(User, pk=pk)
    if request.method == 'POST':
        if album:
            try:
                album = Album.objects.get(user=user, pk=album)
            except Album.DoesNotExist:
                return redirect('accounts:photos_all')
        show = request.POST.get('show')
        if album:
            album.show = show
            album.save()
            album.images.update(show=show)
        else:
            user.images.filter(album__isnull=True).update(show=show)
    return redirect('accounts:photos_all', user.pk)


@for_current_user_or_perm('blogs.delete_article', model=Article)
def article_delete_view(request, obj, **kwargs):
    if request.method == 'POST':
        obj.delete()
        return redirect('accounts:article_list', obj.user.pk)
    return render(request, 'blogs/delete.html', {'article': obj})


@ajax_post_only
@for_current_user_or_perm('blogs.delete_article', model=Article)
def article_hide_view(request, obj, **kwargs):
    obj.not_publish = False if obj.not_publish else True
    obj.save()
    return HttpResponse(obj.not_publish)
