# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os

from django.contrib.auth.models import AbstractUser, Group, Permission
from django.db import models
from django.db.models import F
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import ugettext as _

from shop.models import Item
from soccer.models import Team


class Country(models.Model):
    name = models.CharField(_('Имя'), max_length=255)

    class Meta:
        verbose_name = _('Страна')
        verbose_name_plural = _('Страны')

    def __unicode__(self):
        return u'%s' % self.name


class City(models.Model):
    name = models.CharField(_('Имя'), max_length=255)

    class Meta:
        verbose_name = _('Город')
        verbose_name_plural = _('Города')

    def __unicode__(self):
        return '%s' % self.name


def get_avatar_path(instance, filename):
    ext = filename.split('.')[-1]
    return os.path.join(
        'avatars', '%s.%s' % (instance.pk, ext)
    )


class Avatar(models.Model):
    image = models.ImageField(
        _('Изображения'),
        null=True,
        upload_to=get_avatar_path
    )

    class Meta:
        verbose_name = _('Образ')
        verbose_name_plural = _('Образа')


def get_photo_path(instance, filename):
    ext = filename.split('.')[-1]
    return os.path.join(
        'users', '%s.%s' % (instance.pk, ext)
    )


class User(AbstractUser):

    GENDER_TYPES = (
        (0, 'Не скажу'),
        (1, 'мужской'),
        (2, 'женский'),
    )

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    skype = models.CharField(max_length=255, null=True, blank=True)
    avatar = models.ForeignKey(Avatar, verbose_name=_('Образ'), null=True, blank=True,)
    gender = models.IntegerField(_('Пол'), default=0, choices=GENDER_TYPES)
    birthdate = models.DateField(
        _('День рождения'), null=True, blank=True)
    country = models.ForeignKey(Country, verbose_name=_('Страна'), null=True, blank=True)
    city = models.ForeignKey(City, verbose_name=_('Город'), null=True, blank=True)
    team = models.ForeignKey(Team, verbose_name=_('Команда'), null=True, blank=True)
    friends = models.ManyToManyField('self')
    ignore = models.ManyToManyField('self')
    balance = models.IntegerField(default=0)
    descr = models.TextField(verbose_name=_('О себе'), null=True, blank=True)
    messages_to_email = models.BooleanField(default=True)
    notify_email = models.BooleanField(default=False)
    photo = models.ImageField(upload_to=get_photo_path, null=True, blank=True)
    group = models.ForeignKey(Group, related_name='users', null=True, blank=True)
    REQUIRED_FIELDS = ['email', 'avatar']

    def __unicode__(self):
        return self.username

    def save(self, *args, **kwargs):
        if not self.avatar:
            self.avatar = Avatar.objects.first()
        super(User, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')
        db_table = 'Users'

    def get_contacts(self):
        return self.friends.all()

    def get_friends(self):
        return self.friends.all()

    def get_ignores(self):
        return self.ignore.all()

    def cart_sum(self):
        return self.carts.aggregate(sum=Sum(F('quantity') * F('item__price')))['sum']

    def buy_items(self):
        items = []
        for item in self.carts.all():
            user_item, created = UserItem.objects.get_or_create(user=self, item=item.item)
            if not created:
                user_item.quantity += item.quantity
            else:
                user_item.quantity = item.quantity
            user_item.save()

        self.balance -= self.cart_sum()
        self.carts.all().delete()
        self.save()
        return items

    def friend_requests(self):
        return self.request_to.values_list('user_to_id', flat=True)

    def show_non_album_images(self):
        return True if self.images.filter(album__isnull=True, show=True) else False

    def unsorted_album_cover(self):
        last = self.images.filter(album__isnull=True).last()
        if last:
            return last.image.url
        else:
            return '/static/img/folder.png'

    def has_group_perm(self, perm):
        perm = perm.split('.')
        if len(perm) < 2 or not self.group:
            return False

        try:
            self.group.permissions.get(
                content_type__app_label=perm[0], codename=perm[1])
            return True
        except Permission.DoesNotExist:
            return False


class UserLevel(models.Model):
    levels = (
        (0, u'Нет'),
        (1, u'Запасной игрок'),
        (2, u'Основной игрок'),
        (3, u'Капитан'),
        (4, u'Судья'),
        (5, u'Корреспондент'),
        (6, u'Кандидат в эксперты серебро'),
        (7, u'Кандидат в эксперты золото'),
        (50, u'Эксперт'),
        (101, u'Дисквалифицированный')
    )
    user = models.OneToOneField(User, related_name='level')
    level = models.PositiveSmallIntegerField(choices=levels, default=0, verbose_name=_('Уровень'))
    date = models.DateTimeField(auto_now_add=True)
    prev_points = models.IntegerField(default=0)
    points = models.IntegerField(default=0)

    class Meta:
        verbose_name = _('Уровень пользователя')
        verbose_name_plural = _('Уровни пользователя')

    def __unicode__(self):
        return self.get_level_display()

    def get_points_diff(self):
        return self.points - self.prev_points

    get_points_diff.short_description = 'Заработанные очки'
    get_points_diff.allow_tags = True


class Message(models.Model):
    re = models.ForeignKey('self', null=True, blank=True, related_name='re_set')
    user_to = models.ForeignKey(User, related_name='messages_inbox')
    user_from = models.ForeignKey(User, related_name='messages_sent')
    date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(default=0)
    subject = models.CharField(max_length=255, null=True, blank=True)
    events = models.TextField(null=True, blank=True)
    text = models.TextField()

    @staticmethod
    def get_inbox_messages(user):
        return user.messages_inbox.all().exclude(user_from__in=user.ignore.all()).filter(re__isnull=True)

    @staticmethod
    def get_spam_messages(user):
        return user.messages_inbox.all().filter(user_from__in=user.ignore.all())

    @staticmethod
    def get_sent_messages(user):
        return user.messages_sent.all()


class FriendRequest(models.Model):
    user_from = models.ForeignKey(User, related_name='request_to')
    user_to = models.ForeignKey(User, related_name='request_from')
    time = models.DateTimeField(auto_now_add=True)


class UserLocation(models.Model):
    user = models.OneToOneField(User, related_name='location')
    lat = models.CharField(max_length=30)
    lng = models.CharField(max_length=30)
    title = models.CharField(max_length=100)
    zoom = models.IntegerField(null=True, blank=True)


class UserBan(models.Model):
    user = models.OneToOneField(User, related_name='ban')
    expired = models.DateTimeField()
    active = models.BooleanField(default=True)
    last_ban = models.DateTimeField(null=True, blank=True)

    def records_tag(self):
        result = ''
        for record in self.records.all():
            result += '<p>%s</p>' % record.reason

        return result

    records_tag.short_description = 'Бан пользователя'
    records_tag.allow_tags = True


class BanRecord(models.Model):
    user = models.ForeignKey(User, related_name='ban_records')
    ban = models.ForeignKey(UserBan, related_name='records')
    reason = models.TextField()
    date = models.DateTimeField()


class Event(models.Model):
    pass


class UserItem(models.Model):
    item = models.ForeignKey(Item, related_name='users',
                             verbose_name='Предмет')
    user = models.ForeignKey(User, related_name='items')
    quantity = models.PositiveSmallIntegerField(default=1,
                                                verbose_name='Количество')

    class Meta:
        unique_together = ('item', 'user')
        verbose_name = u'Предмет пользователя'
        verbose_name_plural = u'Предметы пользователя'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.item.admin_only:
            self.quantity = 1
        return super(UserItem, self).save()


class UserGift(models.Model):
    item = models.ForeignKey(Item, related_name='gifts')
    user = models.ForeignKey(User, related_name='gifts')
    user_from = models.ForeignKey(User, related_name='donated')
    comment = models.TextField()
    date = models.DateTimeField(auto_now_add=True)


class Album(models.Model):
    user = models.ForeignKey(User, related_name='albums')
    cover = models.ImageField(upload_to='album_covers/', null=True, blank=True,
                              verbose_name='Обложка альбома',
                              help_text='Фото будет отображаться в виде обложки')
    name = models.CharField(max_length=255, verbose_name='Название альбома')
    show = models.BooleanField(default=True)


def get_image_upload_path(instance, filename):
    if instance.album:
        album = instance.album.name
    else:
        album = 'None'
    return os.path.join(
        'albums', instance.user.username, album, filename
    )


class AlbumImage(models.Model):
    album = models.ForeignKey(Album, null=True, blank=True,
                              related_name='images')
    user = models.ForeignKey(User, related_name='images')
    image = models.ImageField(upload_to=get_image_upload_path)
    title = models.CharField(max_length=255, null=True, blank=True)
    text = models.TextField(null=True, blank=True)
    show = models.BooleanField(default=True)

    def __unicode__(self):
        return self.title


UserFriendsThrough = User.friends.through
UserIgnoreThrough = User.ignore.through


@receiver(post_save, sender=User, dispatch_uid='create_user_level')
def create_user_level(sender, instance, created, **kwargs):
    if created:
        UserLevel.objects.create(user=instance)
