# coding=utf-8
from django import forms
from robokassa.forms import RobokassaForm


class CustomRobokassaForm(RobokassaForm):

    def __init__(self, *args, **kwargs):
        super(CustomRobokassaForm, self).__init__(*args, **kwargs)
        self.fields['OutSum'].widget = forms.NumberInput()
        self.fields['InvId'].required = False
        self.fields['OutSum'].label = u'Сумма'

    def clean(self):
        data = self.cleaned_data
        self.initial['OutSum'] = data['OutSum']
        return data

    def update_signature(self):
        data = self.cleaned_data
        self.fields['InvId'].initial = data.get('InvId')
        data['SignatureValue'] = self._get_signature()
        return data
