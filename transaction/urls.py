from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.payment_view, name='payment'),
    url(r'^robokassa/$', views.result_payment_view, name='payment_result')
]
