# coding=utf-8
from __future__ import unicode_literals

from django.db import models

from users.models import User


class Log(models.Model):
    _log_status = (
        ('awaiting', 'В ожидании'),
        ('success', 'Успешно'),
        ('failed', 'Ошибка'),
        ('rejected', 'Отменен')
    )
    user = models.ForeignKey(User, related_name='transactions')
    amount = models.FloatField()
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=_log_status, default='awaiting')
