# coding=utf-8
import json
import decimal

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render

from transaction.forms import CustomRobokassaForm
from transaction.models import Log


def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError


@login_required
def payment_view(request):
    form = CustomRobokassaForm(initial={
        'Desc': u'Пополнение счета на bobsoccer.ru',
        'Culture': u'ru',
        'OutSum': request.POST.get('sum', None)
    })
    print(request.POST)

    if request.method == 'POST' and request.is_ajax():
        form = CustomRobokassaForm(request.POST, initial={
            'Desc': u'Пополнение счета на bobsoccer.ru',
            'Culture': u'ru',
        })

        if form.is_valid():
            log = Log.objects.create(user=request.user, amount=form.cleaned_data.get('OutSum'))
            form.cleaned_data['InvId'] = log.id
            data = form.update_signature()
            return JsonResponse(json.dumps(data, default=decimal_default), safe=False)

    return render(request, 'payment.html', {'form': form})


def success_payment_view(request):
    print(request.POST)


def fail_payment_view(request):
    print(request.POST)


def result_payment_view(request):
    print(request.POST)
