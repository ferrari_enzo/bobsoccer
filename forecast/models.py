# coding=utf-8
from __future__ import unicode_literals

from django.db import models
from django.db.models import Sum

from soccer.models import Team
from users.models import User


class Tournament(models.Model):
    name = models.CharField(max_length=255)
    date_start = models.DateField()
    date_finish = models.DateField()
    finished = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def get_users(self):
        return User.objects.filter(
            forecasts__match__in=self.matches.values_list('id')).distinct()

    def participants(self):
        return self.get_users().count()

    def update_points(self):
        self.forecast_points.all().delete()
        forecast_points = []

        for user in self.get_users():
            score = difference = result = miss = 0
            for match in self.matches.filter(finished=True):
                try:
                    forecast = match.forecasts.get(user=user)
                except Forecast.DoesNotExist:
                    continue

                team1 = match.team1_score
                team2 = match.team2_score
                team1_user = forecast.team1
                team2_user = forecast.team2
                if team1 == team1_user and team2 == team2_user:
                    score += 1
                elif team1 - team2 == team1_user - team2_user:
                    difference += 1
                elif team1 > team2 and team1_user > team2_user or \
                                        team1 < team2 and team1_user < team2_user:
                    result += 1
                else:
                    miss += 1

            total = score * 3 + difference * 2 + result
            forecast_points.append(ForecastPoint(
                user=user, tournament=self,
                score=score, difference=difference, result=result,
                miss=miss, total=total))

        ForecastPoint.objects.bulk_create(forecast_points)


class Match(models.Model):
    tournament = models.ForeignKey(Tournament, related_name='matches')
    team1 = models.ForeignKey(Team, related_name='matches_team1')
    team2 = models.ForeignKey(Team, related_name='matches_team2')
    team1_score = models.PositiveIntegerField(null=True, blank=True)
    team2_score = models.PositiveIntegerField(null=True, blank=True)
    date = models.DateField()
    time = models.TimeField()
    finished = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s vs %s' % (self.team1.name, self.team2.name)


class Forecast(models.Model):
    user = models.ForeignKey(User, related_name='forecasts')
    match = models.ForeignKey(Match, related_name='forecasts')
    team1 = models.PositiveIntegerField()
    team2 = models.PositiveIntegerField()

    def __unicode__(self):
        return '%s (%s:%s)' % (self.match.__unicode__(), self.team1, self.team2)


class ForecastPointQueryset(models.QuerySet):
    def total_scores(self):
        return self.aggregate(sum=Sum('score'))['sum']


class ForecastPoint(models.Model):
    user = models.ForeignKey(User, related_name='forecast_points')
    tournament = models.ForeignKey(Tournament, related_name='forecast_points')
    score = models.PositiveIntegerField(default=0)
    difference = models.PositiveIntegerField(default=0)
    result = models.PositiveIntegerField(default=0)
    miss = models.PositiveIntegerField(default=0)
    total = models.PositiveIntegerField(default=0)
