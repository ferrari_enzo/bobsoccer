from django import template

from forecast.models import Forecast

register = template.Library()


@register.simple_tag
def is_forecast(user, match):
    if user.is_anonymous():
        return None

    try:
        return match.forecasts.get(user=user)
    except Forecast.DoesNotExist:
        return None


@register.filter
def get_match_points(match, forecast):
    if match.team1_score == forecast.team1 and match.team2_score == forecast.team2:
        return 3
    elif match.team1_score - match.team2_score == forecast.team1 - forecast.team2:
        return 2
    elif match.team1_score > match.team2_score and forecast.team1 > forecast.team2 or \
                            match.team1_score < match.team2_score and forecast.team1 < forecast.team2:
        return 1
    else:
        return 0
