from django.shortcuts import render, redirect, get_object_or_404

from forecast.forms import TournamentForm, match_formset, match_scores_formset
from forecast.models import Match, Tournament, Forecast, ForecastPoint


def forecast_view(request):
    context = {
        'tournaments': Tournament.objects.all()
    }
    return render(request, 'forecast.html', context)


def forecast_result_view(request, pk):
    tournament = get_object_or_404(Tournament, pk=pk)
    context = {
        'tournament': tournament,
        'tournaments': Tournament.objects.exclude(pk=pk),
        'points': ForecastPoint.objects.all()
    }
    return render(request, 'forecast_result.html', context)


def forecast_other_result_view(request):
    return render(request, 'forecast_result.html')


def forecast_create_view(request):
    form = TournamentForm(request.POST or None)
    formset = match_formset(request.POST or None)
    if form.is_valid() and formset.is_valid():
        tournament = form.save()

        matches = []
        for form in formset:
            matches.append(Match(
                tournament=tournament, date=form.cleaned_data.get('date'),
                time=form.cleaned_data.get('time'),
                team1=form.cleaned_data.get('team1'),
                team2=form.cleaned_data.get('team2')
            ))
        Match.objects.bulk_create(matches)
        return redirect('forecast:forecast')

    context = {
        'form': form,
        'formset': formset,
        'tournaments': Tournament.objects.all()
    }
    return render(request, 'competition_create.html', context)


def forecast_update_view(request, pk):
    obj = get_object_or_404(Tournament, pk=pk)
    form = TournamentForm(request.POST or None, instance=obj)
    formset_initial = obj.matches.values('date', 'time', 'team1', 'team2')
    formset = match_formset(request.POST or None,
                            initial=formset_initial)
    if form.is_valid() and formset.is_valid():
        tournament = form.save()

        matches = []
        for form in formset:
            if form.cleaned_data:
                try:
                    match = Match.objects.get(id=form.cleaned_data.get('id'))
                    match.date = form.cleaned_data.get('date'),
                    match.time = form.cleaned_data.get('time'),
                    match.team1 = form.cleaned_data.get('team1'),
                    match.team2 = form.cleaned_data.get('team2')
                    match.save()
                except Match.DoesNotExist:
                    matches.append(Match(
                        tournament=tournament, date=form.cleaned_data.get('date'),
                        time=form.cleaned_data.get('time'),
                        team1=form.cleaned_data.get('team1'),
                        team2=form.cleaned_data.get('team2')))

        Match.objects.bulk_create(matches)
        return redirect('forecast:forecast')

    context = {
        'form': form,
        'formset': formset,
        'tournaments': Tournament.objects.exclude(pk=obj.pk)
    }
    return render(request, 'competition_create.html', context)


def forecast_set_score(request, pk):
    tournament = get_object_or_404(Tournament, pk=pk)
    initial = [
        {'match': i, 'team1_score': i.team1_score, 'team2_score': i.team2_score} for i in tournament.matches.all()]
    formset = match_scores_formset(request.POST or None, initial=initial)
    if formset.is_valid():

        # TODO: optimize set score by moderators
        for form in formset:
            team1 = form.cleaned_data.get('team1_score')
            team2 = form.cleaned_data.get('team2_score')
            match = form.cleaned_data.get('match')
            match.team1_score = team1
            match.team2_score = team2
            match.finished = True
            match.save()

        # TODO: tournament finished
        if not tournament.matches.filter(
                team1_score__isnull=True, team2_score__isnull=True):
            tournament.finished = True
            tournament.save()
        elif tournament.finished:
            tournament.finished = False
            tournament.save()
        tournament.update_points()
    context = {
        'tournament': tournament,
        'formset': formset
    }

    return render(request, 'forecast_set_score.html', context)


def forecast_detail_view(request, pk):
    obj = get_object_or_404(Tournament, pk=pk)
    context = {
        'object': obj,
    }
    if request.method == 'POST':
        forecasts = []
        for match in obj.matches.all():
            team1 = request.POST.get('team1_%s' % match.id)
            team2 = request.POST.get('team2_%s' % match.id)
            if team1 and team2:
                try:
                    f = Forecast.objects.get(user=request.user, match=match)
                    f.team1 = team1
                    f.team2 = team2
                    f.save()
                except Forecast.DoesNotExist:
                    forecasts.append(Forecast(user=request.user, match=match,
                                              team1=team1, team2=team2))

        Forecast.objects.bulk_create(forecasts)
    return render(request, 'forecast_detail.html', context)


def match_create(request):
    return render(request, 'match_create.html')
