# coding=utf-8
from django import forms
from django.forms import formset_factory

from forecast.models import Match, Tournament
from soccer.models import Team


class TournamentForm(forms.ModelForm):
    class Meta:
        model = Tournament
        fields = '__all__'


class MatchForm(forms.Form):
    team1 = forms.ModelChoiceField(queryset=Team.objects.all(),
                                   required=True, label='')

    team2 = forms.ModelChoiceField(queryset=Team.objects.all(),
                                   required=True, label='')

    date = forms.DateField(label='Дата')
    time = forms.TimeField(label='Время')


class MatchScoreForm(forms.Form):
    match = forms.ModelChoiceField(queryset=Match.objects.all(),
                                   widget=forms.HiddenInput(attrs={'readonly': True}))
    team1_score = forms.IntegerField(label='', required=False)
    team2_score = forms.IntegerField(label='', required=False)


match_formset = formset_factory(MatchForm, extra=1)
match_scores_formset = formset_factory(MatchScoreForm, extra=0, can_delete=False)
