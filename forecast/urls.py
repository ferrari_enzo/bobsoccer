from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.forecast_view, name='forecast'),
    url(r'^create/$', views.forecast_create_view, name='competition_create'),
    url(r'^match_create/$', views.match_create, name='match_create'),
    url(r'^(?P<pk>[0-9])/result/$', views.forecast_result_view, name='forecast_result'),
    url(r'^(?P<pk>[0-9])/update/$', views.forecast_update_view, name='forecast_update'),
    url(r'^(?P<pk>[0-9])/scores/set/$', views.forecast_set_score, name='forecast_set_scores'),
    url(r'^(?P<pk>[0-9])/$', views.forecast_detail_view, name='forecast_detail'),
]
